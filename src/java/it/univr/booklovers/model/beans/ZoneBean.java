package it.univr.booklovers.model.beans;

import it.univr.booklovers.model.ZoneModel;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Marian Solomon
 */
public class ZoneBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     * @return
     */
    protected boolean isValid() {
        return (id != 0 && id != -1);
    }

    private int id;
    private String country;

    private String province;

    /**
     *
     */
    public ZoneBean() {
    }

    /**
     *
     * @param id
     * @param country
     * @param province
     */
    public ZoneBean(int id, String country, String province) {
        this.id = id;
        this.country = country;
        this.province = province;
    }

    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public String getCountry() {
        return country;
    }

    /**
     *
     * @param country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     *
     * @return
     */
    public String getProvince() {
        return province;
    }

    /**
     *
     * @param province
     */
    public void setProvince(String province) {
        this.province = province;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public List<ZoneBean> getAllProvince() {
        return ZoneModel.getAllZone();
    }

}
