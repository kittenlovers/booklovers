package it.univr.booklovers.model.beans;

import it.univr.booklovers.model.UserModel;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 *
 * @author blasco991
 *
 */
public class UserBean implements Serializable {

    private String coordinates;
    private String email;
    private boolean forEdit;
    private int id;
    private String image;
    private String name;
    private String password;
    private String postalCode;
    private String province;
    private String surname;
    private String username;
    private int views;
    private static final String EMPTY = "";
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public UserBean() {
        this.image = "img/profile-img.png";
    }

    /**
     *
     * @return
     */
    public String getCoordinates() {
        return coordinates;
    }

    /**
     *
     * @param coordinates
     */
    public void setCoordinates(String coordinates) {
        this.coordinates = coordinates;
    }

    /**
     *
     * @return
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getImage() {
        return image;
    }

    /**
     *
     * @param image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     */
    public String getPassword() {
        return password;
    }

    /**
     *
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     *
     * @return
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     *
     * @param postalCode
     */
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    /**
     *
     * @return
     */
    public String getProvince() {
        return province;
    }

    /**
     *
     * @param province
     */
    public void setProvince(String province) {
        this.province = province;
    }

    /**
     *
     * @return
     */
    public String getSurname() {
        return surname;
    }

    /**
     *
     * @param surname
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     *
     * @return
     */
    public String getUsername() {
        return username;
    }

    /**
     *
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     *
     * @return
     */
    public int getViews() {
        return views;
    }

    /**
     *
     * @param views
     */
    public void setViews(int views) {
        this.views = views;
    }

    /**
     *
     * @return
     */
    public boolean isLogged() {
        return this.id > 0;
    }

    /**
     *
     * @return
     */
    public boolean isNotLogged() {
        return !isLogged();
    }

    public String validateAction(String action) {
        this.forEdit = action.equals("edit");
        return EMPTY;
    }

    public String validateConfirmPassword(String confirmPassword) throws SQLException {
        if (!Objects.equals(password, confirmPassword)) {
            return "Le due password non coincidono";
        }
        return EMPTY;
    }

    public String validateCoordinates(String coordinates) throws SQLException {
        if (!coordinates.isEmpty()) {
            if (coordinates.length() < 3 || coordinates.length() > 64 || !(Pattern.compile("^(\\-?\\d+(\\.\\d+)?),\\s*(\\-?\\d+(\\.\\d+)?)$", Pattern.CASE_INSENSITIVE).matcher(coordinates).find())) {
                return "Coordinate non valide";
            }
        }
        return EMPTY;
    }

    public String validateEmail(String email) throws SQLException {
        if (email == null || !(Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE).matcher(email).find())) {
            return "L'email non è valida";
        } else if (email.length() > 256) {
            return "L'email è troppo lunga";
        } else if (!forEdit && UserModel.emailIsRegistered(email)) {
            return "L'email <" + email + "> è già registrata nel nostro sistema";
        }
        return EMPTY;
    }

    public String validatePassword(String password) throws SQLException {
        setPassword(password);
        if (forEdit) {
            if (!password.isEmpty() && (password.length() < 3 || password.length() > 256)) {
                return "La password deve essere compresa tra 3 e 256 caratteri";
            }
        } else if (password == null || password.length() < 3 || password.length() > 256) {
            return "La password deve essere compresa tra 3 e 256 caratteri";
        }
        return EMPTY;
    }

    public String validateUsername(String username) throws SQLException {
        if (username == null || (username.length() < 3 || username.length() > 64)) {
            return "L'username deve essere compreso tra 3 e 64 caratteri";
        } else if (!forEdit && UserModel.usernameIsRegistered(username)) {
            return "L'username <" + username + "> è già registrata nel nostro sistema";
        }
        return EMPTY;
    }

    public static String validateName(String name) {
        if (name == null || (name.length() < 3 || name.length() > 64)) {
            return "Il nome deve essere compreso tra 3 e 64 caratteri";
        }
        return EMPTY;
    }

    public static String validatePostalCode(String postalCode) {
        if (postalCode == null || !(postalCode.matches("[0-9]+")) || postalCode.length() != 5) {
            return "Il Cap non è valido";
        }
        return EMPTY;
    }

    public static String validateProvince(String province) throws SQLException {
        if (province == null || province.isEmpty()) {
            return "Seleziona la provincia";
        }
        return EMPTY;
    }

    public static String validateSurname(String surname) {
        if (surname == null || surname.length() < 3 || surname.length() > 64) {
            return "Il cognome deve essere compreso tra 3 e 64 caratteri";
        }
        return EMPTY;
    }
}
