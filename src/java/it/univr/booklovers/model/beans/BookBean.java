package it.univr.booklovers.model.beans;

import it.univr.booklovers.model.BookModel;
import it.univr.booklovers.model.UserModel;
import java.sql.SQLException;
import java.util.*;

/**
 *
 * @author blasco991
 *
 */
public class BookBean {

    private String author;
    private String description;
    private String genre;
    private int id;
    private List<String> images = new ArrayList<>();
    private String isbn;
    private String language;
    private UserBean owner;
    private String publisher;
    private String title;
    private int views;
    private int year = Calendar.getInstance().get(Calendar.YEAR);
    private static final String EMPTY = "";

    /**
     *
     * @return
     */
    public String getAuthor() {
        return author;
    }

    /**
     *
     * @param author
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     *
     * @return
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     */
    public String getGenre() {
        return genre;
    }

    /**
     *
     * @param genre
     */
    public void setGenre(String genre) {
        this.genre = genre;
    }

    /**
     *
     * @return @throws java.sql.SQLException
     */
    public List<String> getGenres() throws SQLException {
        return BookModel.getAllGenre();
    }

    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public List<String> getImages() {
        return Collections.unmodifiableList(images);
    }

    /**
     *
     * @param images
     */
    public void setImages(List<String> images) {
        this.images = images;
    }

    /**
     *
     * @return
     */
    public String getIsbn() {
        return isbn;
    }

    /**
     *
     * @param isbn
     */
    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    /**
     *
     * @return
     */
    public String getLanguage() {
        return language;
    }

    /**
     *
     * @param language
     */
    public void setLanguage(String language) {
        this.language = language;
    }

    /**
     *
     * @return
     */
    public UserBean getOwner() {
        return owner;
    }

    /**
     *
     * @param ownerID
     * @throws java.sql.SQLException
     */
    public void setOwner(String ownerID) throws SQLException {
        this.owner = UserModel.getUser(Integer.parseInt(ownerID));
    }

    /**
     *
     * @param owner
     */
    public void setOwner(UserBean owner) {
        this.owner = owner;
    }

    /**
     *
     * @return
     */
    public String getPublisher() {
        return publisher;
    }

    /**
     *
     * @param publisher
     */
    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    /**
     *
     * @return
     */
    public String getThumb() {
        if (images.size() > 0) {
            return images.get(0);
        } else {
            return "img/image-not-found.png";
        }
    }

    /**
     *
     * @return
     */
    public String getTitle() {
        return title;
    }

    /**
     *
     * @param title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     *
     * @return
     */
    public int getViews() {
        return views;
    }

    /**
     *
     * @param views
     */
    public void setViews(int views) {
        this.views = views;
    }

    /**
     *
     * @return
     */
    public int getYear() {
        return year;
    }

    /**
     *
     * @param year
     */
    public void setYear(int year) {
        this.year = year;
    }

    /**
     *
     * @param year
     */
    public void setYear(String year) {
        setYear(year.isEmpty() ? this.year :Integer.parseInt(year));
    }

    public static String validateAuthor(String author) {
        if (author == null || (author.length() < 3 || author.length() > 64)) {
            return "L'autore deve essere compreso tra 3 e 255 caratteri";
        }
        return EMPTY;
    }

    public static String validateDescription(String description) {
        if (description.length() > 200000) {
            return "La descrizione è troppo lunga";
        }
        return EMPTY;
    }

    public static String validateGenre(String genre) {
        if (genre == null || (genre.length() < 3 || genre.length() > 64)) {
            return "Il genere deve essere compreso tra 3 e 64 caratteri";
        }
        return EMPTY;
    }

    public static String validateIsbn(String isbn) {
        if (isbn == null || isbn.length() != 13) {
            return "L'isbn deve essere composto da 13 numeri";
        }
        return EMPTY;
    }

    public static String validateLanguage(String language) {
        if (language == null || (language.length() < 3 || language.length() > 32)) {
            return "La lingua deve essere compreso tra 3 e 32 caratteri";
        }
        return EMPTY;
    }

    public static String validateOwner(String ownerID) throws Exception {
        if (ownerID == null || Integer.parseInt(ownerID) < 1) {
            throw new Exception("OwnerID invalido durante l'inserimento di un libro");
        }
        return EMPTY;
    }

    public static String validatePublisher(String publisher) {
        if (publisher == null || (publisher.length() < 3 || publisher.length() > 64)) {
            return "L'editore deve essere compreso tra 3 e 64 caratteri";
        }
        return EMPTY;
    }

    public static String validateTitle(String title) {
        if (title == null || (title.length() < 1 || title.length() > 64)) {
            return "Il title deve essere compreso tra 3 e 64 caratteri";
        }
        return EMPTY;
    }

    public static String validateYear(String year) {
        if (year == null || year.isEmpty()) {
            return "Anno obbligatorio";
        }
        try {
            if (Integer.parseInt(year) > Calendar.getInstance().get(Calendar.YEAR)) {
                return "I libri che viaggiano nel tempo non ci piacciono";
            }
        } catch (NumberFormatException exception) {
            return "I libri che viaggiano nel tempo non ci piacciono";
        }
        return EMPTY;
    }
}
