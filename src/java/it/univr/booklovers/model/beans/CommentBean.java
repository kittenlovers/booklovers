package it.univr.booklovers.model.beans;

/**
 *
 * @author blasco991
 *
 */
public class CommentBean {

    private BookBean book;
    private String content;
    private String date_time;
    private int id;
    private UserBean user;

    /**
     *
     * @return
     */
    public BookBean getBook() {
        return book;
    }

    /**
     *
     * @param book
     */
    public void setBook(BookBean book) {
        this.book = book;
    }

    /**
     *
     * @return
     */
    public String getContent() {
        return content;
    }

    /**
     *
     * @param content
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     *
     * @return
     */
    public String getDate_time() {
        return date_time;
    }

    /**
     *
     * @param date_time
     */
    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }

    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public UserBean getUser() {
        return user;
    }

    /**
     *
     * @param user
     */
    public void setUser(UserBean user) {
        this.user = user;
    }
}
