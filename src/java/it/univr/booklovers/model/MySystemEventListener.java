package it.univr.booklovers.model;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 *
 * @author Marian Solomon
 */
@WebListener
public class MySystemEventListener implements ServletContextListener {

    /**
     *
     * @param sce
     */
    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        try {
            ConnectionModel.cleanUp();
        } catch (Throwable ex) {
            Logger.getLogger(MySystemEventListener.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param sce
     */
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        try {
            Model.getConnection();
        } catch (Throwable ex) {
            Logger.getLogger(MySystemEventListener.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
