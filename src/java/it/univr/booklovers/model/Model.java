package it.univr.booklovers.model;

import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author Marian Solomon
 */
public abstract class Model {

    private static String pathToProperties = "../resource/config.properties";

    public static Connection getConnection() throws SQLException {
        return ConnectionModel.getConnection(pathToProperties);
    }

    public static void setPathToProperties(String path) {
        pathToProperties = path;
    }

}
