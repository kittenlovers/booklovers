package it.univr.booklovers.model;

import it.univr.booklovers.model.beans.BookBean;
import it.univr.booklovers.model.beans.CommentBean;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Marian Solomon
 */
public abstract class BookModel extends Model {

    /**
     *
     * @param bookBean
     * @throws SQLException
     */
    public static void editBook(BookBean bookBean) throws SQLException {
        String query = "UPDATE book SET isbn = ? , title = ?, language = ?, year = ?, author = ?, "
                + "publisher = ?, genre = ?, owner_id = ?, description = ?"
                + "WHERE id = ? ;";
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(query)) {
            preparedStatement.setString(1, (bookBean.getIsbn()));
            preparedStatement.setString(2, bookBean.getTitle());
            preparedStatement.setString(3, bookBean.getLanguage());
            preparedStatement.setInt(4, bookBean.getYear());
            preparedStatement.setString(5, bookBean.getAuthor());
            preparedStatement.setString(6, bookBean.getPublisher());
            preparedStatement.setString(7, bookBean.getGenre());
            preparedStatement.setInt(8, bookBean.getOwner().getId());
            preparedStatement.setString(9, bookBean.getDescription());
            preparedStatement.setInt(10, bookBean.getId());
            preparedStatement.executeUpdate();
        }
    }

    /**
     *
     * @return @throws SQLException
     */
    public static List<String> getAllAuthor() throws SQLException {
        List<String> result = new ArrayList<>();
        final String query = "SELECT DISTINCT author FROM book ORDER BY author ASC";
        try (Statement statement = getConnection().createStatement();
                ResultSet resultSet = statement.executeQuery(query)) {
            while (resultSet.next()) {
                result.add(resultSet.getString("author"));
            }
        }
        return result;
    }

    /**
     *
     * @return @throws SQLException
     */
    public static List<BookBean> getAllBook() throws SQLException {
        List<BookBean> result = new ArrayList<>();
        String query = "SELECT * FROM book ORDER BY title ASC";
        try (Statement statement = getConnection().createStatement();
                ResultSet resultSet = statement.executeQuery(query)) {
            while (resultSet.next()) {
                result.add(makeBookBean(resultSet));
            }
        }
        return result;
    }

    /**
     *
     * @param owner_id
     * @return
     * @throws SQLException
     */
    public static List<BookBean> getAllBook(int owner_id) throws SQLException {
        List<BookBean> result = new ArrayList<>();
        String query = "SELECT * FROM book WHERE owner_id = ?";
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(query)) {
            preparedStatement.setInt(1, owner_id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    result.add(makeBookBean(resultSet));
                }
            }
        }
        return result;
    }

    /**
     *
     * @return @throws java.sql.SQLException
     */
    public static List<String> getAllGenre() throws SQLException {
        List<String> result = new ArrayList<>();
        String query = "SELECT DISTINCT genre FROM book ORDER BY genre ASC";
        try (Statement statement = getConnection().createStatement();
                ResultSet resultSet = statement.executeQuery(query)) {
            while (resultSet.next()) {
                result.add(resultSet.getString("genre"));
            }
        }
        return result;
    }

    /**
     *
     * @return @throws SQLException
     */
    public static List<String> getAllPublisher() throws SQLException {
        List<String> result = new ArrayList<>();
        String query = "SELECT DISTINCT publisher FROM book ORDER BY publisher ASC";
        try (
                Statement statement = getConnection().createStatement();
                ResultSet resultSet = statement.executeQuery(query)) {
            while (resultSet.next()) {
                result.add(resultSet.getString("publisher"));
            }
        }
        return result;
    }

    /**
     *
     * @return @throws SQLException
     */
    public static List<String> getAllTitle() throws SQLException {
        List<String> result = new ArrayList<>();
        String query = "SELECT DISTINCT title FROM book ORDER BY title ASC";
        try (Statement statement = getConnection().createStatement();
                ResultSet resultSet = statement.executeQuery(query)) {
            while (resultSet.next()) {
                result.add(resultSet.getString("title"));
            }
        }
        return result;
    }

    /**
     *
     * @param id
     * @return
     * @throws SQLException
     */
    public static BookBean getBook(int id) throws SQLException {
        BookBean result = null;
        String query = "SELECT * FROM book WHERE id = ?";
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(query)) {
            preparedStatement.setInt(1, id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    result = makeBookBean(resultSet);
                    updateBookView(id);
                }
            }
        }
        return result;
    }

    /**
     *
     * @param criteria
     * @return
     * @throws SQLException
     */
    public static List<BookBean> getBooks(Map<String, String> criteria) throws SQLException {
        List<BookBean> books = new ArrayList<>();

        String query = "SELECT * FROM book as b ";

        if (!criteria.isEmpty()) {

            if (criteria.containsKey("profile")) {
                query += "JOIN account as a ON b.owner_id = a.id ";
            }

            int params = criteria.size();
            query += "WHERE ";

            if (criteria.containsKey("title")) {
                query += "title ILIKE ? ";
                params--;
            }

            if (criteria.containsKey("year")) {
                if (params != criteria.size()) {
                    query += "AND ";
                }
                query += "year = ? ";
                params--;
            }

            if (criteria.containsKey("author")) {
                if (params != criteria.size()) {
                    query += "AND ";
                }
                query += "author ILIKE ? ";
                params--;
            }

            if (criteria.containsKey("publisher")) {
                if (params != criteria.size()) {
                    query += "AND ";
                }
                query += "publisher ILIKE ? ";
                params--;
            }

            if (criteria.containsKey("genre")) {
                if (params != criteria.size()) {
                    query += "AND ";
                }
                query += "genre ILIKE ? ";
                params--;
            }

            if (criteria.containsKey("profile")) {
                if (params != criteria.size()) {
                    query += "AND ";
                }
                query += "username ILIKE ? ";
                params--;
            }
        }

        try (PreparedStatement preparedStatement = getConnection().prepareStatement(query)) {
            int params = 0;

            if (criteria.containsKey("title")) {
                params++;
                preparedStatement.setString(params, '%' + criteria.get("title") + '%');
            }

            if (criteria.containsKey("year")) {
                params++;
                preparedStatement.setInt(params, Integer.parseInt(criteria.get("year")));
            }

            if (criteria.containsKey("author")) {
                params++;
                preparedStatement.setString(params, '%' + criteria.get("author") + '%');
            }

            if (criteria.containsKey("publisher")) {
                params++;
                preparedStatement.setString(params, '%' + criteria.get("publisher") + '%');
            }

            if (criteria.containsKey("genre")) {
                params++;
                preparedStatement.setString(params, '%' + criteria.get("genre") + '%');
            }

            if (criteria.containsKey("profile")) {
                params++;
                preparedStatement.setString(params, '%' + criteria.get("profile") + '%');
            }

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    books.add(makeBookBean(resultSet));
                }
            }
        }
        return books;
    }

    /**
     *
     * @param book_id
     * @return
     * @throws SQLException
     */
    public static List<CommentBean> getComments(int book_id) throws SQLException {
        List<CommentBean> result = new ArrayList<>();
        String query = "SELECT * FROM comment WHERE book_id = ? ORDER BY id ASC";
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(query)) {
            preparedStatement.setInt(1, book_id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    result.add(makeCommentBean(resultSet));
                }
            }
        }
        return result;
    }

    /**
     *
     * @param numberOfBooks
     * @return
     * @throws SQLException
     */
    public static List<BookBean> getLastNBooks(int numberOfBooks) throws SQLException {
        List<BookBean> result = new ArrayList<>();
        String query = "SELECT * FROM book ORDER BY id DESC LIMIT " + numberOfBooks;
        try (Statement statement = getConnection().createStatement();
                ResultSet resultSet = statement.executeQuery(query)) {
            while (resultSet.next()) {
                result.add(makeBookBean(resultSet));
            }
        }
        return result;
    }

    /**
     *
     * @param limit
     * @return
     * @throws SQLException
     */
    public static List<BookBean> getMostViewedBooks(int limit) throws SQLException {
        List<BookBean> result = new ArrayList<>();
        String query = "SELECT * FROM book ORDER BY views desc LIMIT ?";
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(query)) {
            preparedStatement.setInt(1, limit);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    result.add(makeBookBean(resultSet));
                }
            }
        }
        return result;
    }

    /**
     *
     * @param limit
     * @return
     * @throws SQLException
     */
    public static List<BookBean> getMostViewedBooksByUser(int limit, int account_id) throws SQLException {
        List<BookBean> result = new ArrayList<>();
        String query = "SELECT * FROM book as b JOIN account as a ON b.owner_id = a.id WHERE a.id = ? ORDER BY b.views desc LIMIT ?";
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(query)) {
            preparedStatement.setInt(1, account_id);
            preparedStatement.setInt(2, limit);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    result.add(makeBookBean(resultSet));
                }
            }
        }
        return result;
    }

    /**
     *
     * @param bookBean
     * @throws SQLException
     */
    public static void insertBook(BookBean bookBean) throws SQLException {
        String query = "INSERT INTO book(isbn, title, language, year, author, publisher, genre, owner_id, description)"
                + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);";
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, (bookBean.getIsbn()));
            preparedStatement.setString(2, bookBean.getTitle());
            preparedStatement.setString(3, bookBean.getLanguage());
            preparedStatement.setInt(4, bookBean.getYear());
            preparedStatement.setString(5, bookBean.getAuthor());
            preparedStatement.setString(6, bookBean.getPublisher());
            preparedStatement.setString(7, bookBean.getGenre());
            preparedStatement.setInt(8, bookBean.getOwner().getId());
            preparedStatement.setString(9, bookBean.getDescription());
            int insert = preparedStatement.executeUpdate();

            if (insert != 1) {
                throw new SQLException("Creating user failed, no rows affected.");
            }
            try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    bookBean.setId(generatedKeys.getInt(1));
                } else {
                    throw new SQLException("Creating user failed, no ID obtained.");
                }
            }
        }
    }

    /**
     *
     * @param list
     * @param bookId
     * @throws SQLException
     */
    public static void insertBookImages(List<String> list, int bookId) throws SQLException {
        removeBookImages(bookId);
        for (String image : list) {
            String query = "INSERT INTO book_image(image, book_id) VALUES (?, ?);";
            try (PreparedStatement preparedStatement = getConnection().prepareStatement(query)) {
                preparedStatement.setString(1, image);
                preparedStatement.setInt(2, bookId);
                preparedStatement.executeUpdate();
            }
        }
    }

    /**
     *
     * @param commentBean
     * @throws SQLException
     */
    public static void insertComment(CommentBean commentBean) throws SQLException {
        String query = "INSERT INTO comment(content, date_time, book_id, account_id) VALUES (?, ?, ?, ?);";

        try (PreparedStatement preparedStatement = getConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, (commentBean.getContent()));
            preparedStatement.setString(2, commentBean.getDate_time());
            preparedStatement.setInt(3, commentBean.getBook().getId());
            preparedStatement.setInt(4, commentBean.getUser().getId());
            int insert = preparedStatement.executeUpdate();

            if (insert != 1) {
                throw new SQLException("Creating comment failed, no rows affected.");
            }
            try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    commentBean.setId(generatedKeys.getInt(1));
                } else {
                    throw new SQLException("Creating user failed, no ID obtained.");
                }
            }
        }
    }

    /**
     *
     * @param id
     * @throws SQLException
     */
    public static void removeBook(int id) throws SQLException {
        removeBookImages(id);
        String query = "DELETE FROM book " + " WHERE id = ?";
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(query)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        }
    }

    private static List<String> getBookImages(int bookId) throws SQLException {
        List<String> result = new ArrayList<>();
        String query = "SELECT * FROM book_image WHERE book_id = ? ORDER BY id ASC";
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(query)) {
            preparedStatement.setInt(1, bookId);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    result.add(resultSet.getString("image"));
                }
            }
        }
        return result;
    }

    private static BookBean makeBookBean(ResultSet resultSet) throws SQLException {
        BookBean bookBean = new BookBean();
        bookBean.setId(resultSet.getInt("id"));
        bookBean.setIsbn(resultSet.getString("isbn"));
        bookBean.setTitle(resultSet.getString("title"));
        bookBean.setLanguage(resultSet.getString("language"));
        bookBean.setViews(resultSet.getInt("views"));
        bookBean.setYear(resultSet.getInt("year"));
        bookBean.setAuthor(resultSet.getString("author"));
        bookBean.setGenre(resultSet.getString("genre"));
        bookBean.setPublisher(resultSet.getString("publisher"));
        bookBean.setOwner(UserModel.getUser(resultSet.getInt("owner_id")));
        bookBean.setDescription(resultSet.getString("description"));
        bookBean.setImages(getBookImages(resultSet.getInt("id")));
        return bookBean;
    }

    private static CommentBean makeCommentBean(ResultSet resultSet) throws SQLException {
        CommentBean commentBean = new CommentBean();
        commentBean.setId(resultSet.getInt("id"));
        commentBean.setDate_time(resultSet.getString("date_time"));
        commentBean.setContent(resultSet.getString("content"));
        commentBean.setUser(UserModel.getUser(resultSet.getInt("account_id")));
        commentBean.setBook(BookModel.getBook(resultSet.getInt("book_id")));
        return commentBean;
    }

    private static void removeBookImages(int ID) throws SQLException {
        String query = "DELETE FROM book_image WHERE book_id = ?";

        try (PreparedStatement preparedStatement = getConnection().prepareStatement(query)) {
            preparedStatement.setInt(1, ID);
            preparedStatement.executeUpdate();
        }
    }

    private static void updateBookView(int book_id) throws SQLException {
        String query = "UPDATE book SET views = views + 1 WHERE id = ?";
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(query)) {
            preparedStatement.setInt(1, book_id);
            preparedStatement.execute();
        }
    }

}
