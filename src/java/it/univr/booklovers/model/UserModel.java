package it.univr.booklovers.model;

import it.univr.booklovers.model.beans.UserBean;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Marian Solomon
 */
public abstract class UserModel extends Model {

    private static final long serialVersionUID = 1L;

    /**
     *
     * @param userBean
     * @throws SQLException
     */
    public static void editUser(UserBean userBean) throws SQLException {
        String query = "UPDATE account"
                + " SET name=?, surname=?, email=?, username=?, password=?, postal_code=?, coordinates=?, zone=?"
                + "WHERE id=?;";
        try (
                PreparedStatement preparedStatement = getConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, userBean.getName());
            preparedStatement.setString(2, userBean.getSurname());
            preparedStatement.setString(3, userBean.getEmail());
            preparedStatement.setString(4, userBean.getUsername());
            preparedStatement.setString(5, userBean.getPassword());
            preparedStatement.setString(6, userBean.getPostalCode());
            preparedStatement.setString(7, userBean.getCoordinates());
            preparedStatement.setString(8, userBean.getProvince());
            preparedStatement.setInt(9, userBean.getId());
            int insert = preparedStatement.executeUpdate();

            if (insert != 1) {
                throw new SQLException("Creating account failed, no rows affected.");
            }

        }
    }

    /**
     *
     * @param fileSaved
     * @param id
     * @throws SQLException
     */
    public static void editUserImage(String fileSaved, int id) throws SQLException {
        String query = "UPDATE account"
                + " SET image=?"
                + "WHERE id=?;";
        try (
                PreparedStatement preparedStatement = getConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, fileSaved);
            preparedStatement.setInt(2, id);
            int insert = preparedStatement.executeUpdate();

            if (insert != 1) {
                throw new SQLException("Creating account failed, no rows affected.");
            }

        }
    }

    /**
     *
     * @param email
     * @return
     * @throws SQLException
     */
    public static boolean emailIsRegistered(String email) throws SQLException {
        return getUser(email, "email") != null;

    }

    /**
     *
     * @return @throws SQLException
     */
    public static List<UserBean> getAllProfiles() throws SQLException {
        List<UserBean> result = new ArrayList<>();
        String query = "SELECT * FROM account ORDER BY id ASC";

        try (Statement statement = getConnection().createStatement()) {
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                result.add(makeUserBean(resultSet));
            }
        }
        return result;
    }

    /**
     *
     * @return @throws SQLException
     */
    public static List<String> getAllUsernames() throws SQLException {
        List<String> result = new ArrayList<>();
        String query = "SELECT DISTINCT username FROM account ORDER BY username ASC";

        try (Statement statement = getConnection().createStatement();
                ResultSet resultSet = statement.executeQuery(query);) {
            while (resultSet.next()) {
                result.add(resultSet.getString("username"));
            }
        }
        return result;
    }

    /**
     *
     * @param limit
     * @return
     * @throws SQLException
     */
    public static List<UserBean> getMostViewedUsers(int limit) throws SQLException {
        List<UserBean> result = new ArrayList<>();
        String query = "SELECT * FROM account ORDER BY views desc LIMIT ?";
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(query)) {
            preparedStatement.setInt(1, limit);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    result.add(makeUserBean(resultSet));
                }
            }
        }
        return result;
    }

    /**
     *
     * @param id
     * @return
     * @throws SQLException
     */
    public static UserBean getUser(int id) throws SQLException {
        UserBean result = null;
        String query = "SELECT * FROM account WHERE id = ?";

        try (PreparedStatement preparedStatement = getConnection().prepareStatement(query)) {
            preparedStatement.setInt(1, id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    result = makeUserBean(resultSet);
                }
            }
        }
        return result;
    }

    /**
     *
     * @param username
     * @return
     * @throws SQLException
     */
    public static UserBean getUser(String username) throws SQLException {
        UserBean result = null;
        String query = "SELECT * FROM account WHERE username = ?";

        try (PreparedStatement preparedStatement = getConnection().prepareStatement(query)) {
            preparedStatement.setString(1, username);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    result = makeUserBean(resultSet);
                }
            }
        }
        return result;
    }

    /**
     *
     * @param value
     * @param field
     * @return
     * @throws SQLException
     */
    public static UserBean getUser(final String value, final String field) throws SQLException {
        UserBean result = null;
        final String query;
        if (field.equals("email")) {
            query = "SELECT * FROM account WHERE email = ?";
        } else {
            query = "SELECT * FROM account WHERE username = ?";
        }
        try (
                PreparedStatement preparedStatement = getConnection().prepareStatement(query)) {
            preparedStatement.setString(1, value);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    result = makeUserBean(resultSet);
                }
            }
        }
        return result;
    }

    /**
     *
     * @param userBean
     * @throws SQLException
     */
    public static void insertUser(UserBean userBean) throws SQLException {
        String query = "INSERT INTO account(name, surname, email, username, password, postal_code, coordinates, zone, image) "
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);";

        try (PreparedStatement preparedStatement = getConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, userBean.getName());
            preparedStatement.setString(2, userBean.getSurname());
            preparedStatement.setString(3, userBean.getEmail());
            preparedStatement.setString(4, userBean.getUsername());
            preparedStatement.setString(5, userBean.getPassword());
            preparedStatement.setInt(6, Integer.parseInt(userBean.getPostalCode()));
            preparedStatement.setString(7, userBean.getCoordinates());
            preparedStatement.setString(8, userBean.getProvince());
            preparedStatement.setString(9, userBean.getImage());
            int insert = preparedStatement.executeUpdate();

            if (insert != 1) {
                throw new SQLException("Creating account failed, no rows affected.");
            }

            try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    userBean.setId(generatedKeys.getInt(1));
                } else {
                    throw new SQLException("Creating account failed, no ID obtained.");
                }
            }
        }
    }

    public static void updateUserView(int book_id) throws SQLException {
        String query = "UPDATE account SET views = views + 1 WHERE id = ?";
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(query)) {
            preparedStatement.setInt(1, book_id);
            preparedStatement.execute();
        }
    }

    /**
     *
     * @param username
     * @return
     * @throws SQLException
     */
    public static boolean usernameIsRegistered(String username) throws SQLException {
        return getUser(username, "username") != null;
    }

    private static UserBean makeUserBean(ResultSet resultSet) throws SQLException {
        UserBean accountBean = new UserBean();
        accountBean.setId(resultSet.getInt("id"));
        accountBean.setName(resultSet.getString("name"));
        accountBean.setSurname(resultSet.getString("surname"));
        accountBean.setUsername(resultSet.getString("username"));
        accountBean.setPassword(resultSet.getString("password"));
        accountBean.setEmail(resultSet.getString("email"));
        accountBean.setViews(resultSet.getInt("views"));
        accountBean.setImage(resultSet.getString("image"));
        accountBean.setPostalCode(resultSet.getString("postal_code"));
        accountBean.setCoordinates(resultSet.getString("coordinates"));
        accountBean.setProvince(resultSet.getString("zone"));
        return accountBean;
    }

}
