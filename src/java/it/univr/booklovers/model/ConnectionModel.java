package it.univr.booklovers.model;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.AbstractMap.SimpleEntry;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Marian Solomon
 */
public class ConnectionModel {

    private static final HashMap<Integer, Map.Entry<Integer, Connection>> connectionPool = new HashMap<>();

    private static volatile ConnectionModel model;
    private static String pathToProperties;
    private static final Properties properties = new Properties();
    private static final long serialVersionUID = 1L;

    private ConnectionModel() throws SQLException {
        buildModel();
    }

    private void buildModel() throws SQLException {
        try {
            if (pathToProperties.startsWith("../")) { //relative means that i'm in the container (eg tomcat)
                try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(pathToProperties)) {
                    properties.load(inputStream);
                }
            } else {
                try (InputStream inputStream = new FileInputStream(pathToProperties)) {
                    properties.load(inputStream);
                }
            }
        } catch (IOException exception) {
            Logger.getLogger(ConnectionModel.class.getName()).log(Level.SEVERE, null, exception);
        }
        try {
            Class.forName(properties.getProperty("dbmanager.driver"));
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ConnectionModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        int poolSize = Integer.parseInt(properties.getProperty("db.pool.size"));
        connectionPool.clear();

        for (int i = 0; i < poolSize; i++) {
            connectionPool.put(i, new SimpleEntry<>(0, DriverManager.getConnection(
                    properties.getProperty("dburl")
                    + '/' + properties.getProperty("dbname") + '?' + properties.getProperty("enconding"),
                    properties.getProperty("username"),
                    properties.getProperty("password"))
            ));
        }
    }

    /**
     *
     * @return @throws java.sql.SQLException
     */
    private final synchronized Connection getConnect() throws SQLException {
        int currentMinUsage = Integer.MAX_VALUE;
        int currentMin = 0;

        for (Map.Entry<Integer, Map.Entry<Integer, Connection>> entrySet : connectionPool.entrySet()) {
            int id = entrySet.getKey();
            Entry<Integer, Connection> value = entrySet.getValue();
            if (value.getKey() < currentMinUsage) {
                if (!value.getValue().isClosed()) {
                    currentMin = id;
                    currentMinUsage = value.getKey();
                }
            }
        }
        Connection connection = connectionPool.get(currentMin).getValue();
        if (connection.isClosed()) {
            buildModel();
            return getConnection(pathToProperties);
        } else {
            return connection;
        }
    }

    /**
     *
     * @param path
     * @return @throws java.sql.SQLException
     */
    public static Connection getConnection(String path) throws SQLException {
        if (model == null) {
            pathToProperties = path;
            model = new ConnectionModel();
            model.getConnect();
        }
        return model.getConnect();
    }

    /**
     *
     * @throws Throwable
     */
    protected static void cleanUp() throws Throwable {
        for (Entry<Integer, Entry<Integer, Connection>> connection : connectionPool.entrySet()) {
            connection.getValue().getValue().close();
        }
    }

}
