package it.univr.booklovers.model;

import it.univr.booklovers.model.beans.ZoneBean;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Marian Solomon
 */
public class ZoneModel implements Serializable {

    private static final List<ZoneBean> province = new ArrayList<>();
    private static final long serialVersionUID = 1L;
    private static volatile ZoneModel zoneModel;

    private ZoneModel() {
        try (BufferedReader br = new BufferedReader(new FileReader(getClass().getClassLoader().getResource("../resource/zone.csv").getFile()))) {
            String line = br.readLine();
            for (int i = 0; line != null; line = br.readLine(), i++) {
                String[] item = line.split(";");
                province.add(new ZoneBean(i, item[0], item[1]));
            }
        } catch (IOException e) {
            Logger.getLogger(ZoneModel.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    /**
     *
     * @return
     */
    public static List<ZoneBean> getAllZone() {
        if (zoneModel != null) {
            return Collections.unmodifiableList(province);
        } else {
            zoneModel = new ZoneModel();
            return getAllZone();
        }
    }
}
