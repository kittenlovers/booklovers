package it.univr.booklovers.controller.frontend;

import it.univr.booklovers.controller.ServletModel;
import it.univr.booklovers.model.BookModel;
import it.univr.booklovers.model.UserModel;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ListaServlet
 */
@WebServlet(urlPatterns = {"/search"})
public class SearchServlet extends ServletModel {

    private static final long serialVersionUID = 1L;

    /**
     *
     * @param request
     * @param response
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        Map<String, String> criteria = new HashMap<>();

        String title = request.getParameter("title");
        if (title != null) {
            criteria.put("title", title);
        }
        String genre = request.getParameter("genre");
        if (genre != null) {
            criteria.put("genre", genre);
        }
        String year = request.getParameter("year");
        if (year != null) {
            criteria.put("year", year);
        }
        String author = request.getParameter("author");
        if (author != null) {
            criteria.put("author", author);
        }
        String publisher = request.getParameter("publisher");
        if (publisher != null) {
            criteria.put("publisher", publisher);
        }

        search(request, response, criteria);
    }

    /**
     *
     * @param request
     * @param response
     * @throws java.io.UnsupportedEncodingException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException {
        Map<String, String> criteria = new HashMap<>();

        String title = request.getParameter("title");
        if (title != null) {
            if (!title.isEmpty()) {
                criteria.put("title", title);
                request.setAttribute("title", title);
            }
        }

        String year = request.getParameter("year");
        if (year != null) {
            if (!year.isEmpty()) {
                try {
                    Integer.parseInt(year);
                    criteria.put("year", String.valueOf(Integer.parseInt(year)));
                    request.setAttribute("year", year);
                } catch (NumberFormatException ex) {
                    exceptionHandler(ex, request, response);
                }
            }
        }

        String author = request.getParameter("author");
        if (author != null) {
            if (!author.isEmpty()) {
                criteria.put("author", author);
                request.setAttribute("author", author);
            }
        }

        String publisher = request.getParameter("publisher");
        if (publisher != null) {
            if (!publisher.isEmpty()) {
                criteria.put("publisher", publisher);
                request.setAttribute("publisher", publisher);
            }
        }

        String genre = request.getParameter("genre");
        if (genre != null) {
            if (!genre.isEmpty()) {
                criteria.put("genre", genre);
                request.setAttribute("genre", genre);
            }
        }

        String profile = request.getParameter("profile");
        if (profile != null) {
            if (!profile.isEmpty()) {
                criteria.put("profile", profile);
                request.setAttribute("profile", profile);
            }
        }

        search(request, response, criteria);
    }

    /**
     *
     * @param criterias
     */
    private void search(HttpServletRequest request, HttpServletResponse response, Map<String, String> criteria) {
        List<String> years = new ArrayList<>();
        for (int i = 0; i < 2015; i++) {
            years.add(String.valueOf(i));
        }
        request.setAttribute("years", stringListToJsonArray(years));

        try {
            request.setAttribute("profiles", stringListToJsonArray(UserModel.getAllUsernames()));
            request.setAttribute("titles", stringListToJsonArray(BookModel.getAllTitle()));
            request.setAttribute("authors", stringListToJsonArray(BookModel.getAllAuthor()));
            request.setAttribute("genres", stringListToJsonArray(BookModel.getAllGenre()));
            request.setAttribute("publishers", stringListToJsonArray(BookModel.getAllPublisher()));

            request.setAttribute("books", BookModel.getBooks(criteria));
            forward("frontend/search.jsp", request, response);
        } catch (SQLException ex) {
            exceptionHandler(ex, request, response);
        }
    }

}
