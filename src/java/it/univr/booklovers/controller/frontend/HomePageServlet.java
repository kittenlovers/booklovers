package it.univr.booklovers.controller.frontend;

import it.univr.booklovers.controller.ServletModel;
import it.univr.booklovers.model.BookModel;
import java.sql.SQLException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ListaServlet
 */
@WebServlet(urlPatterns = {"/index.html","/homepage","/about"})
public class HomePageServlet extends ServletModel {

    private static final long serialVersionUID = 1L;

    /**
     *
     * @param request
     * @param response
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        String path = request.getServletPath();
        switch (path) {
            case "/about":
                forward("frontend/about.jsp", request, response);
                break;
            default:
                try {
                    request.setAttribute("lastBooks", BookModel.getLastNBooks(3));
                    forward("frontend/homepage.jsp", request, response);
                } catch (SQLException ex) {
                    exceptionHandler(ex, request, response);
                }
        }
    }

    /**
     *
     * @param req
     * @param resp
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        doGet(req, resp);
    }

}
