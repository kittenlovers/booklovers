package it.univr.booklovers.controller.frontend;

import it.univr.booklovers.controller.ServletModel;
import it.univr.booklovers.model.BookModel;
import it.univr.booklovers.model.UserModel;
import it.univr.booklovers.model.beans.BookBean;
import it.univr.booklovers.model.beans.CommentBean;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ListaServlet
 */
@WebServlet(urlPatterns = {"/book"})
public class BookServlet extends ServletModel {

    private static final long serialVersionUID = 1L;

    /**
     *
     * @param request
     * @param response
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        int id = 0;
        String book_id = request.getParameter("id");
        if (book_id != null && !book_id.isEmpty()) {
            try {
                id = Integer.parseInt(book_id);
            } catch (NumberFormatException ex) {
                exceptionHandler(ex, request, response);
            }
        }
        loadBook(request, response, id);
    }

    /**
     *
     * @param request
     * @param response
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        String action = request.getParameter("action");
        int book_id = 0;
        if (action.equals("comment")) {
            CommentBean commentBean = new CommentBean();
            try {
                book_id = Integer.parseInt(request.getParameter("book_id"));
                commentBean.setBook(BookModel.getBook(book_id));
                commentBean.setUser(UserModel.getUser(Integer.parseInt(request.getParameter("user_id"))));
                commentBean.setContent(request.getParameter("content"));
                DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                Date date = new Date();
                commentBean.setDate_time(dateFormat.format(date));
                if (!commentBean.getContent().isEmpty()) {
                    BookModel.insertComment(commentBean);
                }
            } catch (SQLException ex) {
                exceptionHandler(ex, request, response);
            }
        }
        redirect("book?id=" + book_id, request, response);
    }

    private void loadBook(HttpServletRequest request, HttpServletResponse response, int book_id) {
        try {
            BookBean book = BookModel.getBook(book_id);
            if (book != null) {
                List<CommentBean> comments = BookModel.getComments(book_id);
                request.setAttribute("comments", comments);
                request.setAttribute("book", book);
                forward("frontend/book.jsp", request, response);
            } else {
                redirect("frontend/error_page.jsp", request, response);
            }
        } catch (NullPointerException | SQLException ex) {
            exceptionHandler(ex, request, response);
        }
    }

}
