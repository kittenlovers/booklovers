package it.univr.booklovers.controller.backend;

import it.univr.booklovers.controller.ServletModel;
import it.univr.booklovers.model.BookModel;
import it.univr.booklovers.model.UserModel;
import it.univr.booklovers.model.beans.UserBean;
import java.sql.SQLException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ListaServlet
 */
@WebServlet(urlPatterns = {"/profile"})
public class ProfileServlet extends ServletModel {

    private static final long serialVersionUID = 1L;

    /**
     *
     * @param request
     * @param response
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        UserBean userBean = (UserBean) request.getSession().getAttribute("user");

        if (userBean != null && userBean.isLogged()) {

            int id = 0;
            String profile_id = request.getParameter("id");
            if (profile_id != null && !profile_id.isEmpty()) {
                try {
                    id = Integer.parseInt(profile_id);
                } catch (NumberFormatException ex) {
                    exceptionHandler(ex, request, response);
                }
            }
            String username = request.getParameter("username");
            if (username != null && !username.isEmpty()) {
                try {
                    id = UserModel.getUser(username).getId();
                } catch (SQLException ex) {
                    exceptionHandler(ex, request, response);
                }
            }
            loadProfile(request, response, id);
        } else {
            redirect("account?action=login", request, response);
        }
    }

    /**
     *
     * @param request
     * @param response
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        doGet(request, response);
    }

    /**
     *
     * @param criterias
     */
    private void loadProfile(HttpServletRequest request, HttpServletResponse response, int profile_id) {
        try {
            UserBean profile = UserModel.getUser(profile_id);
            if (profile != null) {
                UserModel.updateUserView(profile.getId());
                request.setAttribute("profile", profile);
                request.setAttribute("books", BookModel.getAllBook(profile.getId()));
                forward("backend/profile.jsp", request, response);
            } else {
                redirect("backend/error_page.jsp", request, response);
            }
        } catch (NullPointerException | SQLException ex) {
            exceptionHandler(ex, request, response);
        }
    }

}
