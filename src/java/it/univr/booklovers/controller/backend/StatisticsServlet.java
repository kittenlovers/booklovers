package it.univr.booklovers.controller.backend;

import it.univr.booklovers.controller.ServletModel;
import it.univr.booklovers.model.BookModel;
import it.univr.booklovers.model.UserModel;
import it.univr.booklovers.model.beans.UserBean;
import java.sql.SQLException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author MarianClaudiu
 */
@WebServlet(urlPatterns = {"/statistics"})
public class StatisticsServlet extends ServletModel {

    private static final long serialVersionUID = 1L;

    /**
     *
     * @param request
     * @param response
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        UserBean userBean = (UserBean) request.getSession().getAttribute("user");

        if (userBean == null || userBean.isNotLogged()) {
            redirect("account?action=login", request, response);
        } else {
            try {
                request.setAttribute("mostViewedBook", BookModel.getMostViewedBooks(4));
                request.setAttribute("mostViewedBookByUser", BookModel.getMostViewedBooksByUser(4, userBean.getId()));
                request.setAttribute("mostViewedUsers", UserModel.getMostViewedUsers(4));
                forward("backend/statistics.jsp", request, response);
            } catch (SQLException ex) {
                exceptionHandler(ex, request, response);
            }
        }
    }

    /**
     *
     * @param request
     * @param response
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        doGet(request, response);
    }

}
