package it.univr.booklovers.controller.backend;

import it.univr.booklovers.controller.ServletModel;
import it.univr.booklovers.model.UserModel;
import it.univr.booklovers.model.beans.UserBean;
import java.sql.SQLException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ListaServlet
 */
@WebServlet(urlPatterns = {"/map"})
public class UsersMapServlet extends ServletModel {

    private static final long serialVersionUID = 1L;

    /**
     *
     * @param request
     * @param response
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        UserBean userBean = (UserBean) request.getSession().getAttribute("user");
        if (userBean != null && userBean.isLogged()) {
            if (userBean.getCoordinates().isEmpty()) {
                request.setAttribute("error", "Per vedere la mappa degli utenti compila le "
                        + "coordinate nel tuo <a href='account?action=edit'>Profilo</a>");
            } else {
                try {
                    request.setAttribute("users", UserModel.getAllProfiles());
                } catch (SQLException ex) {
                    exceptionHandler(ex, request, response);
                }
            }
            forward("backend/usersMap.jsp", request, response);
        } else {
            redirect("account?action=login", request, response);
        }
    }

    /**
     *
     * @param request
     * @param response
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        doGet(request, response);
    }

}
