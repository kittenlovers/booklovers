package it.univr.booklovers.controller.backend;

import it.univr.booklovers.controller.ServletModel;
import it.univr.booklovers.model.BookModel;
import it.univr.booklovers.model.UserModel;
import it.univr.booklovers.model.beans.UserBean;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ListaServlet
 */
@WebServlet(urlPatterns = {"/account"})
@MultipartConfig
public class AccountServlet extends ServletModel {

    private static final int BUFFER_LENGTH = 4096;
    private static final long serialVersionUID = 1L;

    /**
     *
     * @param request
     * @param response
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        String action = request.getParameter("action");
        action = action == null ? "index" : action;
        UserBean userBean = (UserBean) request.getSession().getAttribute("user");
        try {
            if (userBean != null && userBean.isLogged()) {
                request.setAttribute("books", BookModel.getAllBook(userBean.getId()));
                switch (action) {
                    case "edit":
                        request.setAttribute("form_destination", "account?action=" + action);
                        forward("backend/account_form.jsp", request, response);
                        break;
                    case "logout":
                        logout(request, response);
                        break;
                    case "signin":
                    case "login":
                        redirect("account?action=index", request, response);
                        break;
                    default: {
                        forward("backend/account.jsp", request, response);
                    }
                }
            } else {
                switch (action) {
                    case "signin":
                        request.setAttribute("form_destination", "account?action=" + action);
                        forward("backend/account_form.jsp", request, response);
                        break;
                    case "login":
                        forward("backend/login.jsp", request, response);
                        break;
                    default:
                        redirect("account?action=login", request, response);
                }
            }
        } catch (SQLException ex) {
            exceptionHandler(ex, request, response);
        }
    }

    /**
     *
     * @param request
     * @param response
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        String action = request.getParameter("action");
        action = action == null ? "index" : action;
        UserBean userBean = (UserBean) request.getSession().getAttribute("user");
        if (userBean != null && userBean.isLogged()) {
            switch (action) {
                case "edit":
                    edit(request, response);
                    break;
                case "editImage":
                    editImage(request, response);
                    break;
                case "login":
                case "signin":
                default:
                    doGet(request, response);
            }
        } else {
            switch (action) {
                case "signin":
                    signIn(request, response);
                    break;
                case "edit":
                case "login":
                    login(request, response);
                    break;
                default:
                    doGet(request, response);
            }
        }
    }

    private void edit(HttpServletRequest request, HttpServletResponse response) {
        UserBean accountBean = (UserBean) request.getSession().getAttribute("user");
        if (accountBean != null && accountBean.isLogged()) {
            try {
                Map<String, String> errors = validate(request.getParameterMap(), accountBean, UserBean.class);
                if (errors.isEmpty()) {
                    set(request.getParameterMap(), accountBean, UserBean.class);
                    UserModel.editUser(accountBean);
                    request.getSession().setAttribute("user", accountBean);
                    request.setAttribute("books", BookModel.getAllBook(accountBean.getId()));
                    redirect("account?action=index", request, response);

                } else {
                    request.setAttribute("errors", errors);
                    request.setAttribute("form_destination", "account?action=edit");
                    forward("backend/account_form.jsp", request, response);
                }
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | SQLException ex) {
                exceptionHandler(ex, request, response);
            }
        } else {
            redirect("account?action=login", request, response);
        }
    }

    private void editImage(HttpServletRequest request, HttpServletResponse response) {
        String fileSaved = "img/profile-img.png";
        try {
            fileSaved = saveFile(request.getPart("profile-img"), "img/users/");
            UserBean userBean = (UserBean) request.getSession().getAttribute("user");
            userBean.setImage(fileSaved);
            UserModel.editUserImage(fileSaved, userBean.getId());
            int statusCode = 0;
            while (statusCode != 200) {
                URL url = new URL("http://" + request.getServerName() + ':' + request.getServerPort() + request.getContextPath() + '/' + fileSaved);
                HttpURLConnection http = (HttpURLConnection) url.openConnection();
                statusCode = http.getResponseCode();
            }
            redirect("account", request, response);

        } catch (IOException | SQLException | ServletException ex) {
            exceptionHandler(ex, request, response);
        }
    }

    private void login(HttpServletRequest request, HttpServletResponse response) {

        String email = request.getParameter("email");
        String password = request.getParameter("password");

        UserBean accountBean = null;
        try {
            accountBean = UserModel.getUser(email, "email");

            if (accountBean != null && password.equals(accountBean.getPassword())) {
                request.getSession().setAttribute("user", accountBean);
                request.setAttribute("books", BookModel.getAllBook(accountBean.getId()));
                redirect("account?action=index", request, response);
            } else {
                request.setAttribute("errors", "Email o password errati");
                forward("backend/login.jsp", request, response);
            }
        } catch (SQLException ex) {
            exceptionHandler(ex, request, response);
        }
    }

    private void logout(HttpServletRequest request, HttpServletResponse response) {
        request.getSession().removeAttribute("user");
        redirect("homepage", request, response);
    }

    private void signIn(HttpServletRequest request, HttpServletResponse response) {
        UserBean accountBean = (UserBean) request.getSession().getAttribute("user");
        try {
            Map<String, String> errors = validate(request.getParameterMap(), accountBean, UserBean.class);
            set(request.getParameterMap(), accountBean, UserBean.class);
            if (errors.isEmpty()) {
                UserModel.insertUser(accountBean);
                request.getSession().setAttribute("user", accountBean);
                redirect("account?action=index", request, response);
            } else {
                request.setAttribute("errors", errors);
                request.setAttribute("form_destination", "account?action=signin");
                forward("backend/account_form.jsp", request, response);
            }
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | SQLException ex) {
            exceptionHandler(ex, request, response);
        }
    }

}
