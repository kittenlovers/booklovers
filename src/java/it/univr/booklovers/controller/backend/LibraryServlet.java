package it.univr.booklovers.controller.backend;

import it.univr.booklovers.controller.ServletModel;
import it.univr.booklovers.model.BookModel;
import it.univr.booklovers.model.beans.BookBean;
import it.univr.booklovers.model.beans.UserBean;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author MarianClaudiu
 */
@WebServlet(urlPatterns = {"/account/library"})
@MultipartConfig
public class LibraryServlet extends ServletModel {

    private static final long serialVersionUID = 1L;

    /**
     *
     * @param request
     * @param response
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        String action = request.getParameter("action");
        UserBean userBean = (UserBean) request.getSession().getAttribute("user");
        if (userBean == null || userBean.isNotLogged()) {
            redirect("account?action=login", request, response);
        } else if (action != null) {
            if (!action.equals("add_images")) {
                request.getSession().removeAttribute("book");
            }
            if (action.equals("edit")) {
                try {
                    BookBean bookBean = BookModel.getBook(Integer.parseInt(request.getParameter("book_id")));
                    request.getSession().setAttribute("book", bookBean);
                    request.setAttribute("formAction", action);
                    forward("/backend/book/book_form.jsp", request, response);
                } catch (SQLException ex) {
                    exceptionHandler(ex, request, response);
                }
            } else {
                switch (action) {
                    case "add":
                        request.setAttribute("formAction", action);
                        forward("/backend/book/book_form.jsp", request, response);
                        break;
                    case "add_images":
                        forward("/backend/book/book_images_form.jsp", request, response);
                        break;
                    case "remove":
                        removeBook(request, response);
                        break;
                    default:
                        redirect("account?action=index", request, response);
                }
            }
        }
    }

    /**
     *
     * @param request
     * @param response
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        String action = request.getParameter("action");

        if (action != null) {
            switch (action) {
                case "add":
                    addBook(request, response);
                    break;
                case "edit":
                    editBook(request, response);
                    break;
                case "add_images":
                    addBookImages(request, response);
                    break;
                default:
                    doGet(request, response);
            }
        }
    }

    private void addBook(HttpServletRequest request, HttpServletResponse response) {
        BookBean bookBean = (BookBean) request.getSession().getAttribute("book");
        if (bookBean == null) {
            bookBean = new BookBean();
            request.getSession().setAttribute("book", bookBean);
        }
        try {
            Map<String, String> errors = validate(request.getParameterMap(), bookBean, BookBean.class);
            set(request.getParameterMap(), bookBean, BookBean.class);
            if (errors.isEmpty()) {
                BookModel.insertBook(bookBean);
                redirect("account/library?action=add_images", request, response);
            } else {
                request.setAttribute("errors", errors);
                request.setAttribute("formAction", "add");
                forward("/backend/book/book_form.jsp", request, response);
            }
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | SQLException ex) {
            exceptionHandler(ex, request, response);
        }
    }

    private void addBookImages(HttpServletRequest request, HttpServletResponse response) {
        BookBean bookBean = (BookBean) request.getSession().getAttribute("book");
        ArrayList<String> filesSaved = new ArrayList<>();
        try {
            for (Part part : request.getParts()) {
                String result = saveFile(part, "img/books/");
                if (!result.isEmpty()) {
                    filesSaved.add(result);
                }
            }
            for (String fileSaved : filesSaved) {
                int statusCode = 0;
                while (statusCode != 200) {
                    URL url = new URL("http://" + request.getServerName() + ':' + request.getServerPort() + request.getContextPath() + '/' + fileSaved);
                    HttpURLConnection http = (HttpURLConnection) url.openConnection();
                    statusCode = http.getResponseCode();
                }
            }
            int i = 0;
            if (request.getParameterValues("oldImages") != null) {
                for (String oldImage : request.getParameterValues("oldImages")) {
                    filesSaved.add(i, oldImage);
                    i++;
                }
            }
            bookBean.setImages(null);
            bookBean.setImages(filesSaved);
            BookModel.insertBookImages(filesSaved, bookBean.getId());
            redirect("account/library?action=add_images", request, response);
        } catch (IOException | ServletException | SQLException ex) {
            exceptionHandler(ex, request, response);
        }
    }

    private void editBook(HttpServletRequest request, HttpServletResponse response) {
        try {
            BookBean bookBean = (BookBean) request.getSession().getAttribute("book");
            Map<String, String> errors = validate(request.getParameterMap(), bookBean, BookBean.class
            );
            set(request.getParameterMap(), bookBean, BookBean.class
            );
            if (errors.isEmpty()) {
                BookModel.editBook(bookBean);
                redirect("account/library?action=add_images", request, response);
            } else {
                request.setAttribute("formAction", "edit");
                request.setAttribute("errors", errors);
                forward("/backend/book/book_form.jsp", request, response);
            }
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | SQLException ex) {
            exceptionHandler(ex, request, response);
        }
    }

    private void removeBook(HttpServletRequest request, HttpServletResponse response) {
        String book_id = request.getParameter("book_id");
        try {
            int id = Integer.parseInt(book_id);
            BookModel.removeBook(id);
            redirect("account?action=index", request, response);
        } catch (NumberFormatException | SQLException ex) {
            exceptionHandler(ex, request, response);
        }

    }

}
