package it.univr.booklovers.controller.backend;

import it.univr.booklovers.controller.ServletModel;
import it.univr.booklovers.model.Model;
import java.io.*;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author blasco991
 */
@WebServlet(urlPatterns = {"/load-data"})
public class DataLoadServlet extends ServletModel {

    private static final long serialVersionUID = 1L;

    /**
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        executeQuery(loadQuery("../resource/sql/create_db.sql"));
        response.getOutputStream().println("Resetting DB...");
        executeQuery(loadQuery("../resource/sql/insert_user.sql"));
        response.getOutputStream().println("Insert Users...");
        executeQuery(loadQuery("../resource/sql/insert_book.sql"));
        response.getOutputStream().println("Insert Books...");
        executeQuery(loadQuery("../resource/sql/insert_book_image.sql"));
        response.getOutputStream().println("Insert Book Images...");
        executeQuery(loadQuery("../resource/sql/insert_comment.sql"));
        response.getOutputStream().println("Insert Book Comments...");
    }

    private String loadQuery(String file) throws UnsupportedEncodingException, FileNotFoundException {
        final StringBuilder builder = new StringBuilder();
        String pathToFile = getClass().getClassLoader().getResource(file).getFile();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(pathToFile), Charset.forName("UTF-8")))) {
            String line = br.readLine();
            for (int i = 0;
                    line != null; line = br.readLine(), i++) {
                builder.append(line);
            }
        } catch (IOException ex) {
            Logger.getLogger(DataLoadServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        return builder.toString();
    }

    private static void executeQuery(final String query) {
        try (Statement statement = Model.getConnection().createStatement()) {
            statement.executeUpdate(query);
        } catch (SQLException ex) {
            Logger.getLogger(DataLoadServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
