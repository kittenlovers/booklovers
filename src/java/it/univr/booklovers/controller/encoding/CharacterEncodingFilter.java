package it.univr.booklovers.controller.encoding;

import java.io.IOException;
import java.nio.charset.Charset;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;

/**
 *
 * @author MarianClaudiu
 */
@WebFilter
public class CharacterEncodingFilter implements Filter {

    @Override
    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        request.setCharacterEncoding(Charset.forName("UTF-8").name());
        response.setCharacterEncoding(Charset.forName("UTF-8").name());
        chain.doFilter(request, response);
    }

    @Override
    public void init(FilterConfig config) throws ServletException {
    }
}
