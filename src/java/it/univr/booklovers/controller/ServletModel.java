package it.univr.booklovers.controller;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import org.json.simple.JSONValue;

/**
 *
 * @author Marian Solomon
 */
public abstract class ServletModel extends HttpServlet {

    private static final String EMPTY_STRING = "";
    private static final long serialVersionUID = 1L;

    /**
     * Utility method to save a file name from HTTP header content-disposition
     *
     * @param part
     * @param dir
     * @return
     */
    protected String saveFile(Part part, String dir) {
        String contentDisp = part.getHeader("content-disposition");
        String[] tokens = contentDisp.split(";");
        for (String token : tokens) {
            if (token.trim().startsWith("filename")) {
                try {
                    File prjRoot = new File(getServletContext().getRealPath("/")).getParentFile().getParentFile();
                    String fileName = token.substring(token.indexOf('=') + 2, token.length() - 1);
                    if (!fileName.equals(EMPTY_STRING)) {
                        fileName = dir + System.nanoTime() + fileName;
                        String absolutePath = prjRoot + "/web/" + fileName;
                        Path path = Paths.get(absolutePath);
                        Files.createFile(path);
                        part.write(path.toAbsolutePath().toString());
                        return fileName;
                    }
                } catch (IOException ex) {
                    Logger.getLogger(ServletModel.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return EMPTY_STRING;
    }

    /**
     * Utility method to handle all the exceptions that are fired in the servlet
     * implementations
     *
     * @param exception
     * @param request
     * @param response
     */
    protected static void exceptionHandler(Exception exception, HttpServletRequest request, HttpServletResponse response) {
        Logger.getLogger(ServletModel.class.getName()).log(Level.SEVERE, null, exception);
        forward("/frontend/error_page.jsp", request, response);
    }

    protected static void exceptionHandler(SQLException exception, HttpServletRequest request, HttpServletResponse response) {
        for (Throwable throwable : exception) {
            Logger.getLogger(ServletModel.class.getName()).log(Level.SEVERE, null, exception);
        }
        forward("/frontend/error_page.jsp", request, response);
    }

    /**
     * Utility method to forward a httprequest
     *
     * @param next
     * @param request
     * @param response
     */
    protected static void forward(String next, HttpServletRequest request, HttpServletResponse response) {
        try {
            request.getRequestDispatcher(next).forward(request, response);
        } catch (ServletException | IOException ex) {
            Logger.getLogger(ServletModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Utility method to redirect a httprequest
     *
     * @param target
     * @param request
     * @param response
     */
    protected static void redirect(String target, HttpServletRequest request, HttpServletResponse response) {
        try {
            response.sendRedirect(request.getContextPath() + '/' + target);
        } catch (IOException ex) {
            Logger.getLogger(ServletModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    protected static void set(Map<String, String[]> parameterMap, Object bean, Class classType) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        for (Map.Entry<String, String[]> entrySet : parameterMap.entrySet()) {
            String key = entrySet.getKey();
            String value = entrySet.getValue()[0];
            try {
                Method method = classType.getMethod("set".concat(Character.toUpperCase(key.charAt(0)) + key.substring(1)), String.class);
                method.invoke(bean, value);
            } catch (NoSuchMethodException ex) {
            }
        }
    }

    /**
     *
     * @param list
     * @return
     */
    protected static String stringListToJsonArray(List<String> list) {
        return JSONValue.toJSONString(list);
    }

    protected static Map<String, String> validate(Map<String, String[]> parameterMap, Object bean, Class classType) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        Map<String, String> errors = new HashMap<>();
        for (Map.Entry<String, String[]> entrySet : parameterMap.entrySet()) {
            String key = entrySet.getKey();
            String value = entrySet.getValue()[0];
            try {
                Method method = classType.getMethod("validate".concat(Character.toUpperCase(key.charAt(0)) + key.substring(1)), String.class);
                String result = (String) method.invoke(bean, value);
                if (!result.isEmpty()) {
                    errors.put(key, result);
                }
            } catch (NoSuchMethodException ex) {
            }
        }
        return errors;
    }
}
