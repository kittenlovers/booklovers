package junittest.model;

import it.univr.booklovers.model.Model;
import java.sql.Connection;
import java.sql.SQLException;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;
import org.junit.Test;

/**
 *
 * @author Luca
 */
public class ConnectionModelTest {

    @Test
    public void testGetInstance() throws SQLException {
        Model.setPathToProperties(System.getProperty("user.dir") + "/web/WEB-INF/resource/config.properties");
        assertThat(Model.getConnection(), instanceOf(Connection.class));
    }

}
