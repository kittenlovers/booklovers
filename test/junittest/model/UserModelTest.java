package junittest.model;

import it.univr.booklovers.model.Model;
import it.univr.booklovers.model.UserModel;
import it.univr.booklovers.model.beans.UserBean;
import java.sql.SQLException;
import static org.hamcrest.CoreMatchers.instanceOf;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Luca
 */
public class UserModelTest {

    @Test
    public void testEmailIsNotRegistered() throws SQLException {
        Model.setPathToProperties(System.getProperty("user.dir") + "/web/WEB-INF/resource/config.properties");
        assertFalse(UserModel.emailIsRegistered("fake@mail.com"));
    }

    @Test
    public void testEmailIsRegistered() throws SQLException {
        Model.setPathToProperties(System.getProperty("user.dir") + "/web/WEB-INF/resource/config.properties");
        assertTrue(UserModel.emailIsRegistered("blasco991@gmail.com"));
    }

    @Test
    public void testGetAllProfiles() throws SQLException {
        Model.setPathToProperties(System.getProperty("user.dir") + "/web/WEB-INF/resource/config.properties");
        assertThat(UserModel.getAllProfiles().get(0), instanceOf(UserBean.class));
    }

    @Test
    public void testGetAllUsernames() throws SQLException {
        Model.setPathToProperties(System.getProperty("user.dir") + "/web/WEB-INF/resource/config.properties");
        assertThat(UserModel.getAllUsernames().get(0), instanceOf(String.class));
    }

    @Test
    public void testGetMostViewedUsers() throws SQLException {
        Model.setPathToProperties(System.getProperty("user.dir") + "/web/WEB-INF/resource/config.properties");
        assertThat(UserModel.getMostViewedUsers(4).get(0), instanceOf(UserBean.class));
    }

    @Test
    public void testGetUserById() throws SQLException {
        Model.setPathToProperties(System.getProperty("user.dir") + "/web/WEB-INF/resource/config.properties");
        assertThat(UserModel.getUser(1), instanceOf(UserBean.class));
    }

    @Test
    public void testGetUserByUsername() throws SQLException {
        Model.setPathToProperties(System.getProperty("user.dir") + "/web/WEB-INF/resource/config.properties");
        assertThat(UserModel.getUser("blasco991"), instanceOf(UserBean.class));
    }

    @Test
    public void testUsernameIsNotRegistered() throws SQLException {
        Model.setPathToProperties(System.getProperty("user.dir") + "/web/WEB-INF/resource/config.properties");
        assertFalse(UserModel.usernameIsRegistered("fake"));
    }

    @Test
    public void testUsernameIsRegistered() throws SQLException {
        Model.setPathToProperties(System.getProperty("user.dir") + "/web/WEB-INF/resource/config.properties");
        assertTrue(UserModel.usernameIsRegistered("blasco991"));
    }
}
