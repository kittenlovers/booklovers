package junittest.model;

import it.univr.booklovers.model.BookModel;
import it.univr.booklovers.model.Model;
import it.univr.booklovers.model.beans.BookBean;
import it.univr.booklovers.model.beans.CommentBean;
import java.sql.SQLException;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;
import org.junit.Test;

/**
 *
 * @author Luca
 */
public class BookModelTest {

    @Test
    public void testGetAllAuthor() throws SQLException {
        Model.setPathToProperties(System.getProperty("user.dir") + "/web/WEB-INF/resource/config.properties");
        assertThat(BookModel.getAllAuthor().get(0), instanceOf(String.class));
    }

    @Test
    public void testGetAllBook() throws SQLException {
        Model.setPathToProperties(System.getProperty("user.dir") + "/web/WEB-INF/resource/config.properties");
        assertThat(BookModel.getAllBook().get(0), instanceOf(BookBean.class));
    }

    @Test
    public void testGetAllBookByOwner() throws SQLException {
        Model.setPathToProperties(System.getProperty("user.dir") + "/web/WEB-INF/resource/config.properties");
        assertThat(BookModel.getAllBook(1).get(0), instanceOf(BookBean.class));
    }

    @Test
    public void testGetAllGenre() throws SQLException {
        Model.setPathToProperties(System.getProperty("user.dir") + "/web/WEB-INF/resource/config.properties");
        assertThat(BookModel.getAllGenre().get(0), instanceOf(String.class));
    }

    @Test
    public void testGetAllPublisher() throws SQLException {
        Model.setPathToProperties(System.getProperty("user.dir") + "/web/WEB-INF/resource/config.properties");
        assertThat(BookModel.getAllPublisher().get(0), instanceOf(String.class));
    }

    @Test
    public void testGetAllTitle() throws SQLException {
        Model.setPathToProperties(System.getProperty("user.dir") + "/web/WEB-INF/resource/config.properties");
        assertThat(BookModel.getAllTitle().get(0), instanceOf(String.class));
    }

    @Test
    public void testGetBook() throws SQLException {
        Model.setPathToProperties(System.getProperty("user.dir") + "/web/WEB-INF/resource/config.properties");
        assertThat(BookModel.getBook(1), instanceOf(BookBean.class));
    }

    @Test
    public void testGetComments() throws SQLException {
        Model.setPathToProperties(System.getProperty("user.dir") + "/web/WEB-INF/resource/config.properties");
        assertThat(BookModel.getComments(4).get(0), instanceOf(CommentBean.class));
    }

    @Test
    public void testGetLastNBooks() throws SQLException {
        Model.setPathToProperties(System.getProperty("user.dir") + "/web/WEB-INF/resource/config.properties");
        assertThat(BookModel.getLastNBooks(3).get(0), instanceOf(BookBean.class));
    }

    @Test
    public void testGetMostViewedBooks() throws SQLException {
        Model.setPathToProperties(System.getProperty("user.dir") + "/web/WEB-INF/resource/config.properties");
        assertThat(BookModel.getMostViewedBooks(4).get(0), instanceOf(BookBean.class));
    }

    @Test
    public void testGetMostViewedBooksByUser() throws SQLException {
        Model.setPathToProperties(System.getProperty("user.dir") + "/web/WEB-INF/resource/config.properties");
        assertThat(BookModel.getMostViewedBooksByUser(5, 1).get(0), instanceOf(BookBean.class));
    }
}
