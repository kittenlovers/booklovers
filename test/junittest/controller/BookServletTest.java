package junittest.controller;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author Luca
 */
public class BookServletTest extends TestModel {

    private String url = "http://localhost:8084/BookLovers/book";

    @Test
    public void testGetNotExists() throws IOException {

        url += "/profilo";

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header
        con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0");

        int responseCode = con.getResponseCode();

        assertEquals(404, responseCode);
    }

    @Test
    public void testGetNegativeId() throws IOException {
        url += "?id=-1";

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header
        con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0");

        int responseCode = con.getResponseCode();
        assertEquals(200, responseCode);

        assertEquals("Error", getPageTitle(con));
    }

    @Test
    public void testGetCorrectId() throws IOException {
        url += "?id=1";

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header
        con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0");

        int responseCode = con.getResponseCode();
        assertEquals(200, responseCode);

        assertEquals("Book", getPageTitle(con));
    }

    @Test
    public void testGetIncorrectId() throws IOException {
        url += "?id=500";

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header
        con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0");

        int responseCode = con.getResponseCode();
        assertEquals(200, responseCode);

        assertEquals("Error", getPageTitle(con));
    }

    @Test
    public void testGetTooHighId() throws IOException {
        url += "?id=1000000000000";

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header
        con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0");

        int responseCode = con.getResponseCode();
        assertEquals(200, responseCode);

        assertEquals("Error", getPageTitle(con));
    }
}
