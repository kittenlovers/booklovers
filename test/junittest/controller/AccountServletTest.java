package junittest.controller;

import static org.junit.Assert.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import org.junit.Test;

public class AccountServletTest extends TestModel {

    private String url = "http://localhost:8084/BookLovers/account";

    /**
     *
     * @throws IOException
     */
    @Test
    public void testGetNotExists() throws IOException {

        url += "/profilo";

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header
        con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0");

        int responseCode = con.getResponseCode();

        assertEquals(404, responseCode);
    }

    @Test
    public void testGetEdit() throws IOException {

        url += "?action=edit";

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header
        con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0");

        int responseCode = con.getResponseCode();
        assertEquals(200, responseCode);

        assertEquals("Login", getPageTitle(con));
    }

    @Test
    public void testGetSignin() throws IOException {

        url += "?action=signin";

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header
        con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0");

        int responseCode = con.getResponseCode();

        assertEquals(200, responseCode);

        assertEquals("Registrazione", getPageTitle(con));
    }

    @Test
    public void testGetLogin() throws Exception {

        url += "?action=login";

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header
        con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0");

        int responseCode = con.getResponseCode();

        assertEquals(200, responseCode);

        assertEquals("Login", getPageTitle(con));
    }

    @Test
    public void testGetLogout() throws IOException {

        url += "?action=logout";

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header
        con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0");

        int responseCode = con.getResponseCode();

        assertEquals(200, responseCode);

        assertEquals("Login", getPageTitle(con));
    }
}
