package junittest.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Luca
 */
public abstract class TestModel {

    /**
     * Loops through response headers until Content-Type is found.
     *
     * @param conn
     * @return ContentType object representing the value of the Content-Type
     * header
     */
    protected static ContentType getContentTypeHeader(URLConnection conn) {
        int i = 0;
        boolean moreHeaders = true;
        do {
            String headerName = conn.getHeaderFieldKey(i);
            String headerValue = conn.getHeaderField(i);
            if (headerName != null && headerName.equals("Content-Type")) {
                return new ContentType(headerValue);
            }

            i++;
            moreHeaders = headerName != null || headerValue != null;
        } while (moreHeaders);

        return null;
    }

    protected static Charset getCharset(ContentType contentType) {
        if (contentType != null && contentType.charsetName != null && Charset.isSupported(contentType.charsetName)) {
            return Charset.forName(contentType.charsetName);
        } else {
            return null;
        }
    }

    /**
     * Class holds the content type and charset (if present)
     */
    protected static final class ContentType {

        private static final Pattern CHARSET_HEADER = Pattern.compile("charset=([-_a-zA-Z0-9]+)", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);

        protected String contentType;
        private String charsetName;

        private ContentType(String headerValue) {
            if (headerValue == null) {
                throw new IllegalArgumentException("ContentType must be constructed with a not-null headerValue");
            }
            int n = headerValue.indexOf(";");
            if (n != -1) {
                contentType = headerValue.substring(0, n);
                Matcher matcher = CHARSET_HEADER.matcher(headerValue);
                if (matcher.find()) {
                    charsetName = matcher.group(1);
                }
            } else {
                contentType = headerValue;
            }
        }
    }
    
    protected static String getPageTitle(HttpURLConnection con) throws IOException {
        Pattern TITLE_TAG = Pattern.compile("\\<title>(.*)\\</title>", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);

        ContentType contentType = getContentTypeHeader(con);
        if (!contentType.contentType.equals("text/html")) {
            return null; // don't continue if not HTML
        } else {
            Charset charset = getCharset(contentType);
            if (charset == null) {
                charset = Charset.defaultCharset();
            }
            InputStream in = con.getInputStream();
            StringBuilder content;
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(in, charset))) {
                int n = 0, totalRead = 0;
                char[] buf = new char[1024];
                content = new StringBuilder();
                // read until EOF or first 8192 characters
                while (totalRead < 8192 && (n = reader.read(buf, 0, buf.length)) != -1) {
                    content.append(buf, 0, n);
                    totalRead += n;
                }
            }

            // extract the title
            Matcher matcher = TITLE_TAG.matcher(content);
            if (matcher.find()) {
                /* replace any occurrences of whitespace (which may
                 * include line feeds and other uglies) as well
                 * as HTML brackets with a space */
                return matcher.group(1).replaceAll("[\\s\\<>]+", " ").trim();
            } else {
                return null;
            }
        }
    }
}
