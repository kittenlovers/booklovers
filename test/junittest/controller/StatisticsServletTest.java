package junittest.controller;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Luca
 */
public class StatisticsServletTest extends TestModel{
    private String url = "http://localhost:8084/BookLovers/statistics";
    
    @Test
    public void testGetNotExists() throws IOException {

        url += "/profilo";

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header
        con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0");

        int responseCode = con.getResponseCode();
        assertEquals(404, responseCode);
    }
    
    @Test
    public void testGet() throws IOException {
        
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header
        con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0");

        int responseCode = con.getResponseCode();
        assertEquals(200, responseCode);

        assertEquals("Login", getPageTitle(con));
    }
}
