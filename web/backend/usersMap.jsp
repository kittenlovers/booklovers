<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<t:header>
    <jsp:attribute name="pageTitle">BookLovers.com</jsp:attribute> 
</t:header>

<t:page_content>
    <jsp:body>
        <div class="page-header">
            <h1>Mappa degli utenti</h1>
        </div>
        <div class="row">
            <p class="bg-warning">${error}</p>
            <div class="map_container">
                <div id="map-canvas" class="map_canvas"></div>
            </div>
        </div>
    </jsp:body>

</t:page_content>

<t:footer>
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script src="http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/src/markerclusterer.js"></script>
    <script>
        var map;
        var markerArray = []; //create a global array to store markers
        var windowArray = []; //create a global array to store markers
        var myPoints = [];  //create global array to store points

        $(function () {
        <c:forEach items = "${users}" var = "user" >
            myPoints.push([${user.coordinates}, '<center class="thumbnail"><a href="profile?id=${user.id}"><img class="map-icon" src="${user.image}"/><br/>${user.username}</a></center>']);
        </c:forEach>
        });

        var infowindow = new google.maps.InfoWindow();

        function initialize() {
            var mapOptions = {
                zoom: 6
            };
            map = new google.maps.Map(document.getElementById('map-canvas'),
                    mapOptions);

            var options = {
                map: map,
                position: new google.maps.LatLng(41.29246, 12.5736108),
            };

            map.setCenter(options.position);

            var mcOptions = {
                gridSize: 50,
                maxZoom: 15
            };

            var mc = new MarkerClusterer(map, [], mcOptions);

            // Add markers to the map
            // Set up markers based on the number of elements within the myPoints array
            for (var i = 0; i < myPoints.length; i++) {
                createMarker(new google.maps.LatLng(myPoints[i][0], myPoints[i][1]), myPoints[i][2]);
            }

            mc.addMarkers(markerArray, true);

        }

        function createMarker(latlng, html) {
            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                icon: 'img/books-icon.png',
                animation: google.maps.Animation.DROP,
                zIndex: Math.round(latlng.lat() * -100000) << 5
            });

            google.maps.event.addListener(marker, 'click', function () {
                infowindow.setContent(html);
                infowindow.open(map, this);
            });

            windowArray.push(infowindow);

            markerArray.push(marker);
        }

        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
</t:footer>