<%@page import="it.univr.booklovers.model.beans.ZoneBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<jsp:useBean id="user" class="it.univr.booklovers.model.beans.UserBean" scope="session"/>

<t:header>
    <jsp:attribute name="pageTitle">Statistiche</jsp:attribute>
</t:header>

<t:page_content>
    <jsp:body>
        <h2>I libri più visti sul sito</h2>
        <div class="row">  
            <c:forEach var="bk" items="${mostViewedBook}">
                <div class="col-lg-3 col-md-4 col-sm-5 col-xs-6">
                    <t:book-thumb>
                        <jsp:attribute name="id">${bk.id}</jsp:attribute>
                        <jsp:attribute name="year">${bk.year}</jsp:attribute>
                        <jsp:attribute name="title">${bk.title}</jsp:attribute>
                        <jsp:attribute name="genre">${bk.genre}</jsp:attribute>
                        <jsp:attribute name="author">${bk.author}</jsp:attribute>
                        <jsp:attribute name="copertina">${bk.thumb}</jsp:attribute>
                        <jsp:attribute name="publisher">${bk.publisher}</jsp:attribute>
                        <jsp:attribute name="owner_id">${bk.owner.id}</jsp:attribute>
                        <jsp:attribute name="owner_username">${bk.owner.username}</jsp:attribute>
                        <jsp:body>
                            <ul class="list-group">
                                <li class="list-group-item">N° visualizzazioni<span class="badge">${bk.views}</span></li>
                            </ul>
                        </jsp:body>
                    </t:book-thumb>
                </div>
            </c:forEach>
        </div>
        <h2>I tuoi libri più visti</h2>
        <div class="row">  
            <c:forEach var="bk" items="${mostViewedBookByUser}">
                <div class="col-lg-3 col-md-4 col-sm-5 col-xs-6">
                    <t:book-thumb>
                        <jsp:attribute name="id">${bk.id}</jsp:attribute>
                        <jsp:attribute name="year">${bk.year}</jsp:attribute>
                        <jsp:attribute name="title">${bk.title}</jsp:attribute>
                        <jsp:attribute name="genre">${bk.genre}</jsp:attribute>
                        <jsp:attribute name="author">${bk.author}</jsp:attribute>
                        <jsp:attribute name="copertina">${bk.thumb}</jsp:attribute>
                        <jsp:attribute name="publisher">${bk.publisher}</jsp:attribute>
                        <jsp:attribute name="owner_id">${bk.owner.id}</jsp:attribute>
                        <jsp:attribute name="owner_username">${bk.owner.username}</jsp:attribute>
                        <jsp:body>
                            <ul class="list-group">
                                <li class="list-group-item">N° visualizzazioni<span class="badge">${bk.views}</span></li>
                            </ul>
                        </jsp:body>
                    </t:book-thumb>
                </div>
            </c:forEach>
        </div>
        <h2>I profili più visti sul sito</h2>
        <div class="row">  
            <c:forEach var="profile" items="${mostViewedUsers}">
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-5">
                    <t:user vertical="1">
                        <jsp:attribute name="name">${profile.name}</jsp:attribute>
                        <jsp:attribute name="surname">${profile.surname}</jsp:attribute>
                        <jsp:attribute name="email">${profile.email}</jsp:attribute>
                        <jsp:attribute name="username">${profile.username}</jsp:attribute>
                        <jsp:attribute name="province">${profile.province}</jsp:attribute>
                        <jsp:attribute name="img">${profile.image}</jsp:attribute>
                        <jsp:body>
                            <ul class="list-group">
                                <li class="list-group-item">N° visualizzazioni<span class="badge">${profile.views}</span></li>
                            </ul>
                        </jsp:body>
                    </t:user>
                </div>
            </c:forEach>
        </div>
    </jsp:body>

</t:page_content>

<t:footer></t:footer>