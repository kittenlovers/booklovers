<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<t:header>
    <jsp:attribute name="pageTitle">Profile</jsp:attribute>
</t:header>

<t:page_content>
    <jsp:body>
        <div class="grid">
            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 grid-item">
                <t:user vertical="1">
                    <jsp:attribute name="name">${profile.name}</jsp:attribute>
                    <jsp:attribute name="surname">${profile.surname}</jsp:attribute>
                    <jsp:attribute name="email">${profile.email}</jsp:attribute>
                    <jsp:attribute name="username">${profile.username}</jsp:attribute>
                    <jsp:attribute name="province">${profile.province}</jsp:attribute>
                    <jsp:attribute name="img">${profile.image}</jsp:attribute>
                </t:user>
            </div>
            <c:forEach var="bk" items="${books}">
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 grid-item">
                    <t:book-thumb>
                        <jsp:attribute name="id">${bk.id}</jsp:attribute>
                        <jsp:attribute name="year">${bk.year}</jsp:attribute>
                        <jsp:attribute name="title">${bk.title}</jsp:attribute>
                        <jsp:attribute name="genre">${bk.genre}</jsp:attribute>
                        <jsp:attribute name="author">${bk.author}</jsp:attribute>
                        <jsp:attribute name="copertina">${bk.thumb}</jsp:attribute>
                        <jsp:attribute name="publisher">${bk.publisher}</jsp:attribute>
                        <jsp:attribute name="owner_id">${bk.owner.id}</jsp:attribute>
                        <jsp:attribute name="owner_username">${bk.owner.username}</jsp:attribute>
                    </t:book-thumb>
                </div>
            </c:forEach>
            <c:if test="${empty books}">
                <div class="col-lg-9 col-md-8 col-sm-7 col-xs-9 grid-item lead top-buffer-30">La ricerca non ha prodotto nessun risultato</div>
            </c:if>
        </div>
    </jsp:body>

</t:page_content>

<t:footer></t:footer>