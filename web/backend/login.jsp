<%@page import="it.univr.booklovers.model.beans.ZoneBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<t:header>
    <jsp:attribute name="pageTitle">Login</jsp:attribute>
</t:header>

<t:page_content>
    <jsp:body>
        <div class="top-buffer-100">
            <div class="col-md-6 col-md-offset-1">
                <div class="panel panel-info">
                    <div class="panel-heading">Entra con email e password</div>
                    <div class="panel-body">
                        <form class="form-horizontal" action='account?action=login' method="post">
                            <div class="form-group ${not empty errors ? 'has-error':''}">
                                <label class="col-xs-3 control-label" for="email">E-mail</label>
                                <div class="col-xs-9">
                                    <input type="text" id="email" name="email" value="${user.email}" placeholder="E-mail" class="form-control"/>
                                </div>
                            </div>
                            <div class="form-group ${not empty errors ? 'has-error':''}">
                                <label class="col-xs-3 control-label" for="password">Password</label>
                                <div class="col-xs-9">
                                    <input type="password" id="inputPassword" name="inputPassword" placeholder="Password" class="form-control"/>
                                    <input type="hidden" id="password" name="password" />
                                </div>
                            </div>
                            <button class="btn btn-success col-xs-4 col-xs-offset-4" type="submit">Login</button>
                            <script type="text/javascript">
                                $(function () {
                                    $('#inputPassword').change(function () {
                                        $('#password').val(CryptoJS.SHA512($('#inputPassword').val()));
                                    });
                                });
                            </script>  
                        </form>
                    </div>
                    <div class="panel-footer">
                        <span class="text-danger" ${empty errors ? 'hidden':''}>${errors}</span>&nbsp
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-info">
                    <div class="panel-heading">Registrati!</div>
                    <div class="panel-body">
                        <p class="well-sm">Se non hai ancora un account sul nostro portale crealo subito!</p>
                        <a href="account?action=signin"><button class="btn btn-warning col-xs-4 col-xs-offset-4" type="submit">Registrati</button></a>
                    </div>
                </div>
            </div>
        </div>
    </jsp:body>

</t:page_content>

<t:footer/>