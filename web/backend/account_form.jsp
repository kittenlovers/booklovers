<%@page import="it.univr.booklovers.model.beans.ZoneBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:useBean id="user" class="it.univr.booklovers.model.beans.UserBean" scope="session"/>
<jsp:useBean id="zone" class="it.univr.booklovers.model.beans.ZoneBean" scope="page" />

<t:header>
    <jsp:attribute name="pageTitle">Registrazione</jsp:attribute>
</t:header>

<t:page_content>
    <jsp:body>
        <div class="top-buffer-30">
            <form class="form-horizontal" action='${requestScope.form_destination}' method="post" id="formAccount">
                <div class="form-group ${not empty errors.name ? 'has-error':''}">
                    <label class="col-md-4 col-xs-4 control-label" for="name">Nome</label>
                    <div class="col-md-5 col-xs-8">
                        <input type="text" id="name" name="name" value="${user.name}" maxlength="64" placeholder="Nome" class="form-control">
                        <span class="control-label">${errors.name}</span>
                    </div>
                </div>
                <div class="form-group ${not empty errors.surname ? 'has-error':''}">
                    <label class="col-md-4 col-xs-4 control-label" for="surname">Cognome</label>
                    <div class="col-md-5 col-xs-8">
                        <input type="text" id="surname" name="surname" value="${user.surname}" maxlength="64" placeholder="Cognome" class="form-control">
                        <span class="control-label">${errors.surname}</span>
                    </div>
                </div>  
                <div class="form-group ${not empty errors.postalCode ? 'has-error':''}">
                    <label class="col-md-4 col-xs-4 control-label" for="postalCode">Cap</label>
                    <div class="col-md-5 col-xs-8">
                        <input type="text" id="postalCode" name="postalCode" value="${user.postalCode}" maxlength="32" placeholder="Cap" class="form-control">
                        <span class="control-label">${errors.postalCode}</span>
                    </div>
                </div>  
                <div class="form-group ${not empty errors.province ? 'has-error':''}">
                    <label class="col-md-4 col-xs-4 control-label" for="province">Provincia</label>
                    <div class="col-md-5 col-xs-8">
                        <select class="form-control" id="province" name="province" required="required">
                            <option disabled="disabled" selected="selected" value="">Selezione Provincia</option>
                            <c:forEach items="${zone.allProvince}" var="zn">
                                <option value="${zn.province}" ${(user.province == zn.province)?"selected='selected'":""}>${zn.province}</option> 
                            </c:forEach>
                        </select>
                        <span class="control-label">${errors.province}</span>
                    </div>
                </div>
                <div class="form-group ${not empty errors.coordinates ? 'has-error':''}">
                    <label class="col-md-4 col-xs-4 control-label" for="coordinates">Coordinate</label>
                    <div class="col-md-5 col-xs-8">
                        <input type="text" id="coordinates" name="coordinates" value="${user.coordinates}" maxlength="64" placeholder="Coordinate" class="form-control">
                        <span class="control-label">${errors.coordinates}</span>
                    </div>
                    <script>
                        $(document).ready(function () {
                            var x = document.getElementById("coordinates");
                            if (navigator.geolocation) {
                                navigator.geolocation.getCurrentPosition(showPosition, error);
                            } else {
                                x.value = "Geolocalizzazzione fallita";
                            }

                            function showPosition(position) {
                                x.value = position.coords.latitude + ',' + position.coords.longitude;
                            }
                            function error(position) {
                                x.value = "Geolocalizzazzione bloccata dal browser";
                            }
                        });
                    </script>
                </div>
                <div class="form-group ${not empty errors.username ? 'has-error':''}">
                    <label class="col-md-4 col-xs-4 control-label" for="username">Username</label>
                    <div class="col-md-5 col-xs-8">
                        <input type="text" id="username" name="username" value="${user.username}" maxlength="64" placeholder="Username" class="form-control">
                        <span class="control-label">${errors.username}</span>
                    </div>
                </div>
                <div class="form-group ${not empty errors.email ? 'has-error':''}">
                    <label class="col-md-4 col-xs-4 control-label" for="email">E-mail</label>
                    <div class="col-md-5 col-xs-8">
                        <input type="text" id="email" name="email" value="${user.email}" maxlength="256" placeholder="E-mail" class="form-control">
                        <span class="control-label">${errors.email}</span>
                    </div>
                </div>
                <div class="form-group ${not empty errors.password ? 'has-error':''}">
                    <label class="col-md-4 col-xs-4 control-label" for="password">Password</label>
                    <div class="col-md-5 col-xs-8">
                        <input type="password" id="inputPassword" name="inputPassword" placeholder="Password" maxlength="256" class="form-control">
                        <input type="hidden" id="password" name="password"/>
                        <span class="control-label">${errors.password}</span>
                    </div>
                </div>
                <div class="form-group ${not empty errors.confirmPassword ? 'has-error':''}">
                    <label class="col-md-4 col-xs-4 control-label" for="confirmPassword">Password (Confirm)</label>
                    <div class="col-md-5 col-xs-8">
                        <input type="password" id="inputConfirmPassword" name="inputConfirmPassword" placeholder="Conferma Password" maxlength="256" class="form-control">
                        <input type="hidden" id="confirmPassword" name="confirmPassword"/>
                        <span class="control-label">${errors.confirmPassword}</span>
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-success col-xs-8 col-md-4 col-md-offset-4 col-xs-offset-2">Invia</button>
                </div>
                <script type="text/javascript">
                    $(function () {
                        $('#inputPassword').change(function () {
                            $('#password').val(CryptoJS.SHA512($('#inputPassword').val()));
                        });
                        $('#inputConfirmPassword').change(function () {
                            $('#confirmPassword').val(CryptoJS.SHA512($('#inputConfirmPassword').val()));
                        });
                    });
                </script>
            </form>
        </div>
    </jsp:body>

</t:page_content>

<t:footer>
</t:footer>