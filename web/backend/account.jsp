<%@page import="it.univr.booklovers.model.beans.ZoneBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<jsp:useBean id="user" class="it.univr.booklovers.model.beans.UserBean" scope="session"/>

<t:header>
    <jsp:attribute name="pageTitle">BookLovers.com</jsp:attribute>
</t:header>

<t:page_content>
    <jsp:body>
        <div class="row jumbotron">
            <div class="col-md-6 caption">
                <p class="lead">Benvenuto ${user.username}</p>
                <div class="list-group col-lg-5">
                    <a href="account?action=edit"><button type="button" class="list-group-item">Modifica i tuoi dati</button></a>
                    <a href="account/library?action=add"><button type="button" class="list-group-item">Aggiungi libro</button></a>
                    <a href="statistics"><button type="button" class="list-group-item">Vedi le statistiche</button></a>
                </div>
            </div>
            <div class="col-md-6">
                <t:user>
                    <jsp:attribute name="img">${user.image}</jsp:attribute>
                    <jsp:attribute name="name">${user.name}</jsp:attribute>
                    <jsp:attribute name="surname">${user.surname}</jsp:attribute>
                    <jsp:attribute name="email">${user.email}</jsp:attribute>
                    <jsp:attribute name="username">${user.username}</jsp:attribute>
                    <jsp:attribute name="province">${user.province}</jsp:attribute>
                    <jsp:body><br/>
                        <form action="account?action=editImage" method="POST" enctype="multipart/form-data" id="formEditImage">
                            <span class="btn btn-success btn-file">
                                Modifica foto profilo<input type="file" name="profile-img" id="profile-img" onchange="this.form.submit()"/>
                            </span>
                        </form>
                    </jsp:body>
                </t:user>
            </div>
        </div>
        <h2>La tua libreria</h2>
        <c:if test="${empty books}">
            <div class="col-lg-9 col-md-8 col-sm-7 col-xs-9 lead top-buffer-30">La ricerca non ha prodotto nessun risultato</div>
        </c:if>
        <div class="grid">  
            <c:forEach var="bk" items="${books}">
                <div class="col-lg-3 col-md-4 col-sm-5 col-xs-6 grid-item">
                    <t:book-thumb>
                        <jsp:attribute name="id">${bk.id}</jsp:attribute>
                        <jsp:attribute name="year">${bk.year}</jsp:attribute>
                        <jsp:attribute name="title">${bk.title}</jsp:attribute>
                        <jsp:attribute name="genre">${bk.genre}</jsp:attribute>
                        <jsp:attribute name="author">${bk.author}</jsp:attribute>
                        <jsp:attribute name="copertina">${bk.thumb}</jsp:attribute>
                        <jsp:attribute name="publisher">${bk.publisher}</jsp:attribute>
                        <jsp:attribute name="owner_id">${bk.owner.id}</jsp:attribute>
                        <jsp:attribute name="owner_username">${bk.owner.username}</jsp:attribute>
                        <jsp:body>
                            <a href="account/library?action=remove&book_id=${bk.id}" class="btn btn-danger" role="button">Rimuovi</a>
                            <a href="account/library?action=edit&book_id=${bk.id}" class="btn btn-warning" role="button">Modifica</a>
                        </jsp:body>
                    </t:book-thumb>
                </div>
            </c:forEach>
        </div>
    </jsp:body>

</t:page_content>

<t:footer></t:footer>