<%@page import="it.univr.booklovers.model.beans.ZoneBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:useBean id="user" class="it.univr.booklovers.model.beans.UserBean" scope="session"/>
<jsp:useBean id="book" class="it.univr.booklovers.model.beans.BookBean" scope="session"/>

<t:header>
    <jsp:attribute name="pageTitle">BookLovers.com</jsp:attribute>
</t:header>

<t:page_content>
    <jsp:body>
        <form class="form-horizontal" action='account/library?action=${formAction}&book_id=${book.id}' method="post">
            <fieldset>
                <div class="form-group ${not empty errors.title ? 'has-error':''}">
                    <label class="col-sm-2 control-label" for="title">Titolo</label>
                    <div class="col-sm-4">
                        <input type="text" id="title" name="title" value="${book.title}" placeholder="Titolo" class="form-control" maxlength="64">
                        <span class="control-label">${errors.title}</span>
                    </div>
                </div>

                <div class="form-group ${not empty errors.isbn ? 'has-error':''}">
                    <label class="col-sm-2 control-label" for="isbn">Isbn</label>
                    <div class="col-sm-4">
                        <input type="text" id="isbn" name="isbn" value="${book.isbn}" placeholder="Isbn" class="form-control" maxlength="13">
                        <span class="control-label">${errors.isbn}</span>
                    </div>
                </div>  

                <div class="form-group ${not empty errors.language ? 'has-error':''}">
                    <label class="col-sm-2 control-label" for="language">Lingua</label>
                    <div class="col-sm-4">
                        <input type="text" id="language" name="language" value="${book.language}" placeholder="Lingua" class="form-control" maxlength="32">
                        <span class="control-label">${errors.language}</span>
                    </div>
                </div>  

                <div class="form-group ${not empty errors.year ? 'has-error':''}">
                    <label class="col-sm-2 control-label" for="year">Anno</label>
                    <div class="col-sm-4">
                        <input type="number" id="language" name="year" value="${book.year}" maxlength="4" placeholder="Anno" class="form-control">
                        <span class="control-label">${errors.year}</span>
                    </div>
                </div>  

                <div class="form-group ${not empty errors.author ? 'has-error':''}">
                    <label class="col-sm-2 control-label" for="author">Autore</label>
                    <div class="col-sm-4">
                        <input type="text" id="author" name="author" value="${book.author}" placeholder="Autore" class="form-control" maxlength="64">
                        <span class="control-label">${errors.author}</span>
                    </div>
                </div>  

                <div class="form-group ${not empty errors.publisher ? 'has-error':''}">
                    <label class="col-sm-2 control-label" for="publisher">Casa Editrice</label>
                    <div class="col-sm-4">
                        <input type="text" id="publisher" name="publisher" value="${book.publisher}" placeholder="Casa Editrice" class="form-control" maxlength="64">
                        <span class="control-label">${errors.publisher}</span>
                    </div>
                </div>  

                <div class="form-group ${not empty errors.genre ? 'has-error':''}">
                    <label class="col-sm-2 control-label" for="genre">Genere</label>
                    <div class="col-sm-4">
                        <input type="text" id="genre" name="genre" value="${book.genre}" placeholder="Genere" class="form-control" maxlength="64">
                        <span class="control-label">${errors.genre}</span>
                    </div>
                </div>  

                <div class="form-group ${not empty errors.description ? 'has-error':''}">                   
                    <label class="col-sm-2 control-label" for="description">Descrizione</label>
                    <div class="col-sm-4">
                        <textarea id="description" name="description" rows="5" cols="60" placeholder="Descrizione" class="form-control">${book.description}</textarea>
                        <span class="control-label">${errors.description}</span>
                    </div>
                </div>
                <input type="hidden" name="owner" value="${user.id}"/>
                <div class="form-group">
                    <div class="col-sm-4">
                        <button class="btn">Invia</button>
                    </div>
                </div>

            </fieldset>
        </form>
    </jsp:body>

</t:page_content>

<t:footer></t:footer>