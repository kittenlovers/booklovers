<%@page import="it.univr.booklovers.model.beans.ZoneBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:useBean id="book" class="it.univr.booklovers.model.beans.BookBean" scope="session"/>

<t:header>
    <jsp:attribute name="pageTitle">BookLovers.com</jsp:attribute>
</t:header>

<t:page_content>
    <jsp:body>
        <h1 class="page-header"><b>${book.title}</b> | <small>Inserisci le immagini</small></h1>
        <form class="form row grid" action='account/library?action=add_images&book_id=${param.book_id}' method="post" enctype="multipart/form-data" id="imagesForm">
            <div class="zoom-gallery">
                <c:forEach var="image" items="${book.images}">
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 grid-item margin-top-10">
                        <a href="${image}" data-source="${image}" title="${book.title}">
                            <img src="${image}" class="img-responsive"/>
                            <input type="hidden" value="${image}" name="oldImages"/>
                        </a>
                        <span class="glyphicon glyphicon-minus-sign remove-image-book text-danger" aria-hidden="true">
                            <script>
                                $(".remove-image-book").click(function () {
                                    this.parentElement.remove();
                                    $("#imagesForm").submit();
                                });
                            </script>
                        </span>
                    </div>
                </c:forEach>
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 grid-item">
                    <span class="file-input btn btn-primary btn-file top-buffer-100">
                        <span class="glyphicon glyphicon-camera" aria-hidden="true">&nbsp;Browse&hellip;</span>
                        <input type="file" name="thumb" onchange="this.form.submit()" multiple/>  
                    </span>
                </div>
            </div>
        </form>
        <div class="col-md-4 col-xs-offset-6">
            <a href="account?action=index" class="btn btn-warning">Continua</a>
        </div>
    </jsp:body>

</t:page_content>

<t:footer/>