function clearForm(oForm) {

    var elements = oForm.elements;
    oForm.reset();
    for (i = 0; i < elements.length; i++) {

        field_type = elements[i].type.toLowerCase();
        switch (field_type) {

            case "text":
            case "number":
            case "password":
            case "textarea":
            case "hidden":

                elements[i].value = "";
                break;
            case "radio":
            case "checkbox":
                if (elements[i].checked) {
                    elements[i].checked = false;
                }
                break;
            case "select-one":
            case "select-multi":
                elements[i].selectedIndex = -1;
                break;
            default:
                break;
        }
    }
}

$(window).load(function () {
    $(".se-pre-con").delay("fast").fadeOut("slow");
    $('#contents').css('visibility', 'visible').fadeIn("slow");
});

$(function mansory() {
    var $grid = $('.grid').isotope({
        itemSelector: '.grid-item',
        percentPosition: true,
        transitionDuration: '0.3s',
        masonry: {
            columnWidth: '.grid-item'
        }
    });
    $grid.imagesLoaded().progress(function () {
        $grid.isotope('layout');
    });
});

$(document).ready(function () {
    $('.zoom-gallery').magnificPopup({
        delegate: 'a',
        type: 'image',
        closeOnContentClick: false,
        closeBtnInside: false,
        mainClass: 'mfp-with-zoom mfp-img-mobile',
        image: {
            verticalFit: true,
            titleSrc: function (item) {
                return item.el.attr('title') + ' &middot; <a class="image-source-link" href="' + item.el.attr('data-source') + '" target="_blank">image source</a>';
            }
        },
        gallery: {
            enabled: true
        },
        zoom: {
            enabled: true,
            duration: 300, // don't foget to change the duration also in CSS
            opener: function (element) {
                return element.find('img');
            }
        }
    });
});