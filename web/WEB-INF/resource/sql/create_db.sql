DROP TABLE IF EXISTS comment;
DROP TABLE IF EXISTS book_image;  
DROP TABLE IF EXISTS book; 
DROP TABLE IF EXISTS account_relation; 
DROP TABLE IF EXISTS account;

CREATE TABLE IF NOT EXISTS account (
	id SERIAL PRIMARY KEY,
	name VARCHAR(64) not null,
	surname VARCHAR(64) not null,
	email VARCHAR(256) unique not null,
	image VARCHAR(256) not null,
	username VARCHAR(64) unique not null,
	password VARCHAR(256) not null,
	postal_code VARCHAR(32) not null,
	views INT not null DEFAULT 0,
	coordinates VARCHAR(64) not null,
	zone VARCHAR(64)
);

CREATE TABLE IF NOT EXISTS book (
	id SERIAL PRIMARY KEY,
	isbn VARCHAR(13) not null,
	title VARCHAR(64) not null,
        description TEXT not null,
	language VARCHAR(32) not null,
	year INT not null,
	views INT not null DEFAULT 0,
	author VARCHAR(64) not null,
	publisher VARCHAR(64) not null,
	genre VARCHAR(64) not null,
	owner_id INT not null references account(id)
);

CREATE TABLE IF NOT EXISTS comment (
	id SERIAL PRIMARY KEY,
	content TEXT not null,
	date_time VARCHAR(20) not null,
	account_id INT not null references account(id),
	book_id INT not null references book(id)
);

CREATE TABLE IF NOT EXISTS book_image (
	id SERIAL PRIMARY KEY,
	image VARCHAR(255) not null,
	book_id INT references book(id)
);
