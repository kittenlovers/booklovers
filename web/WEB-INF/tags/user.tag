<%@tag description="Header Tag" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%-- The list of normal or fragment attributes can be specified here: --%>
<%@attribute name="name"%>
<%@attribute name="surname"%>
<%@attribute name="username"%>
<%@attribute name="email"%>
<%@attribute name="province"%>
<%@attribute name="img"%>
<%@attribute name="vertical"%> 

<%-- content --%>
<c:choose>
    <c:when test="${vertical == '1'}">
        <div class="thumbnail">
            <img class="img-rounded img-responsive" src="${img}" />
            <div class="caption">
                <h3>${surname} ${name}</h3>
                <c:url value="profile" var="url">
                    <c:param name="username" value="${username}" />
                </c:url>
                <p><a href="${url}">${username}</a></p>
                <p><a href="mailto:${email}">${email}</a></p>
                <p>${zone}</p>
            </div>
            <jsp:doBody/>
        </div>
    </c:when>    
    <c:otherwise>
        <div class="col-lg-5">
            <center>
                <img class="img-rounded img-responsive" src="${img}" alt="${name} ${surname}" id="profile-img"/>
                <jsp:doBody/>
            </center>
        </div>
        <div class="col-lg-7">
            <div class="thumbnail">
                <div class="caption">
                    <h3>${surname} ${name}</h3>
                    <p>${username}</p>
                    <p><a href="mailto:${email}">${email}</a></p>
                    <p>${province}</p>
                </div>
            </div>
        </div>
    </c:otherwise>
</c:choose>    
