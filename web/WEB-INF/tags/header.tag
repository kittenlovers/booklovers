<%@tag description="Header Tag" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@attribute name="pageTitle"%>

<jsp:useBean id="user" class="it.univr.booklovers.model.beans.UserBean" scope="session"/>
<jsp:useBean id="book" class="it.univr.booklovers.model.beans.BookBean" scope="page" />

<!DOCTYPE html>
<html lang="it">
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <title>${pageTitle}</title>
        <base href="/BookLovers/"/>
        <link href="css/bootstrap.css" rel="stylesheet"/>
        <link href="css/jquery-ui.css" rel="stylesheet"/>
        <link href="css/font-awesome.min.css" rel="stylesheet"/>
        <link href="css/custom.css" rel="stylesheet"/>
        <link href="magnific/magnific-popup.css" rel="stylesheet"/>
        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery-ui.js"></script>
        <script src="js/imagesloaded.pkgd.js"></script>
        <script src="js/isotope.pkgd.js"></script>
        <script src="magnific/jquery.magnific-popup.min.js"></script>
        <script src="js/sha512.js"></script>
        <script src="js/custom.js"></script>
        <jsp:doBody/>
    </head>
    <body>
        <header>
            <nav>
                <div class="navbar navbar-inverse navbar-static-top" role="navigation">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href=""><img alt="BookLovers.com" width="30" src="img/booklovers_symbol.png"></a>
                        </div>
                        <div class="collapse navbar-collapse">
                            <ul class="nav navbar-nav">
                                <li><a href="frontend/about.jsp">About</a></li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Generi <span class="caret"></span></a>
                                    <ul class="dropdown-menu navbar-inverse">
                                        <c:forEach var="gn" items="${book.genres}">
                                            <c:url value="search" var="url"><c:param name="genre" value="${gn}"/></c:url>
                                            <li><a href="${url}" class="btn-white">${gn}</a></li> 
                                            </c:forEach>                            
                                    </ul>
                                </li>
                            </ul>
                            <form class="navbar-form navbar-left visible-lg" action="search" method="GET">
                                <div class="form-group">
                                    <input type="text" maxlength="64" class="form-control" placeholder="Search" name="title" required="required">
                                </div>
                                <button type="submit" class="btn btn-default">Cerca</button>
                            </form>
                            <ul class="nav navbar-nav navbar-right">
                                <c:choose>
                                    <c:when test="${user.logged}">
                                        <li><a href="statistics">Statistiche</a></li>
                                        <li><a href="map">BookLoversMap</a></li>
                                        <li><p class="navbar-text">Signed in as <a href="account?action=index" class="text-white">${user.username}</a></p></li>
                                        <li><a href="account?action=logout">Logout</a></li>                        
                                        </c:when>
                                        <c:otherwise>
                                        <li><a href="account?action=login">Login</a></li>
                                        <li><a href="account?action=signin">Registrati</a></li>
                                        </c:otherwise>
                                    </c:choose>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
        </header>
