<%@tag description="Footer Tag" pageEncoding="UTF-8"%>

<footer class="container-fluid navbar-inverse top-buffer-30">
    <div class="text-center margin-20">
        <img src="img/booklovers_logo-200.png" class="img-rounded"/><span class="lead">BookLovers.com&trade; è un marchio registrato</span>
    </div>
</footer>
<jsp:doBody/>
</body>
</html>