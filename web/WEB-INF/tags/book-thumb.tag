<%@tag description="Header Tag" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%-- The list of normal or fragment attributes can be specified here: --%>
<%@attribute name="id"%>
<%@attribute name="title"%>
<%@attribute name="copertina"%>
<%@attribute name="author"%>
<%@attribute name="genre"%>
<%@attribute name="year"%>
<%@attribute name="publisher"%>
<%@attribute name="owner_id"%>
<%@attribute name="owner_username"%>

<%-- content --%>

<div class="thumbnail book-thumb">
    <a href="book?id=${id}" >
        <img src="${copertina}" alt="copertina">
    </a>
    <div class="caption">
        <p><a class="lead" href="book?id=${id}">${title}</a><br/>
        <label>Autore:</label> ${author}<br/>
        <label>Genere:</label> ${genre}<br/>
        <label>Anno:</label> ${year}<br/>
        <label>Editore:</label> ${publisher}<br/>
        by <a href="profile?id=${owner_id}">${owner_username}</a></p>
        <jsp:doBody/>
    </div>
</div>
