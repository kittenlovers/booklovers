<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:header>
    <jsp:attribute name="pageTitle">About</jsp:attribute>
</t:header>

<t:page_content>
    <jsp:body>
        <div class="page-header">
            <h1>About us <small>Who we are</small></h1>
        </div>
        <div class="row text-center">
            <div class="col-sm-4">
                <img src="img/team/marian-solomon.jpg" class="img-circle" />
                <div class="caption">
                    <h3>Marian Solomon</h3>
                </div>
            </div>
            <div class="col-sm-4">
                <img src="img/team/luca-zidda.jpg" class="img-circle" />
                <div class="caption">
                    <h3>Luca Zidda</h3>
                </div> 
            </div>
            <div class="col-sm-4">
                <img src="img/team/cavazza-alvise.jpg" class="img-circle" />
                <div class="caption">
                    <h3>Cavazza Alvise</h3>
                </div>
            </div>
            <div class="row">
                <p class="lead">Kittenlovers è il nostro team di lavoro</p>
            </div>
            <div class="col-lg-6">
                        <img class="img-responsive img-thumbnail" src="img/love-books.jpg"/>
                    </div>
                    <div class="col-lg-6">
                        <img class="img-responsive footer-img center-block" src="img/booklovers_logo-200.png"/>
                        <p class="lead">
                            BookLovers è stato creato con le ultime tecnologie disponibili per risultare il più accessibile possibile.<br/>
                            Il layout del sito è completamente responsive e fruibile anche da dispositivi mobili come smartphone e tablet.<br/>
                            Enjoy yourself!
                        </p>
                        <a href="mailto:info@kittenlovers.com">info@kittenlovers.com</a>.
                    </div>    
        </div>
    </jsp:body>
</t:page_content>

<t:footer/>