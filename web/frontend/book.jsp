<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:useBean id="user" class="it.univr.booklovers.model.beans.UserBean" scope="session"/>

<t:header>
    <jsp:attribute name="pageTitle">Book</jsp:attribute>
</t:header>

<t:page_content>
    <jsp:body>
        <div class="row">
            <div class="page-header">
                <h1>${book.title}</h1>
            </div>
        </div>
        <div class="row page-header grid">
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 grid-item">
                <p class="lead">Scheda Tecnica</p>
                <div class="thumbnail">
                    <div class="caption">
                        <c:url value="search" var="url"><c:param name="author" value="${book.author}"/></c:url>
                        <p><label>Autore:</label> <a href="${url}">${book.author}</a></p>
                            <c:url value="search" var="url"><c:param name="genre" value="${book.genre}"/></c:url>
                        <p><label>Genere:</label> <a href="${url}">${book.genre}</a></p>
                        <p><label>Anno:</label> <a href="search?year=${book.year}">${book.year}</a></p>
                            <c:url value="search" var="url"><c:param name="publisher" value="${book.publisher}"/></c:url>
                        <p><label>Editore:</label> <a href="${url}">${book.publisher}</a></p>
                        <p><label>Lingua:</label> ${book.language} </p>
                        <p><label>Isbn:</label> ${book.isbn}</p>
                        <p>by <a href="profile?id=${book.owner.id}">${book.owner.username}</a></p>
                            <c:if test="${user.logged}">
                            <span class="btn btn-warning">
                                <a href="mailto:${book.owner.email}?subject=Richiesta Scambio libro da BookLoverS.com | Username: ${user.username}&body=Ciao ${book.owner.name}, sono interessato al tuo libro '${book.title}' dai uno sguardo al mio profilo per vedere cosa potrebbe interessarti. Attendo tuo riscontro."
                               target="_blank" class="lead text-white">Richiedi libro</a>
                            </span>
                        </c:if>
                    </div>
                </div>
            </div>
            <c:forEach var="image" items="${book.images}">
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 zoom-gallery grid-item margin-top-10">
                    <a href="${image}" data-source="${image}" title="${book.title}">
                        <img src="${image}" class="img-responsive"/>
                    </a>
                </div>
            </c:forEach>
            <c:if test="${empty book.images}"><div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 zoom-gallery grid-item margin-top-10">
                    <a href="${image}" data-source="${image}" title="${book.title}">
                        <img src="${book.thumb}" class="img-responsive"/>
                    </a>
                </div>
            </c:if>
        </div>
        <div class="panel panel-default page-header">
            <div class="panel-heading">
                <h3 class="panel-title">Descrizione</h3>
            </div>
            <div class="panel-body">
                <p>${book.description}</p>
            </div>
        </div>
        <div class="row">
            <c:choose>
                <c:when test="${user.logged}">
                    <center>
                        <form action="book?action=comment" method="POST">
                            <label class="lead">Inserisci un commento</label><br/>
                            <textarea name="content" rows="3" cols="60"></textarea><br/>
                            <input type="hidden" name="book_id" value="${book.id}">
                            <input type="hidden" name="user_id" value="${user.id}">
                            <button type="submit" class="btn btn-primary">Commenta</button>
                        </form>
                    </center>
                </c:when>
            </c:choose>
            <br/>
            <c:if test="${empty comments}">
                <center class="lead">Non ci sono ancora commenti</center>
                </c:if>
                <c:forEach var="cm" items="${comments}">
                <div class="panel panel-info thumbnail">
                    <div class="panel-heading">
                        <h3 class="panel-title"><label>by</label> <a href="profile?id=${cm.user.id}">${cm.user.username}</a></h3>
                        <small>${cm.date_time}</small>
                    </div>
                    <div class="panel-body">
                        <p>${cm.content}</p>
                    </div>
                </div>
            </c:forEach>
        </div>
    </jsp:body>

</t:page_content>

<t:footer></t:footer>