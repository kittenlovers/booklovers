<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:header>
    <jsp:attribute name="pageTitle">Error</jsp:attribute>
</t:header>

<t:page_content>
    <jsp:body>
        <div class="content">
            <div class="page-header">
                <h1>Errore di sistema</h1>
            </div>
            <div class="row">
                <div class="col-lg-4 jumbotron">
                    <p>
                        Ops, qualcosa non è andato a buon fine.
                        Ti preghiamo di riprovare ad aggiornare la pagine, 
                        se l'errore dovesse persistere provare a ricominciare 
                        la tua navigazione dalla <a href='homepage'>Homepage</a>
                        o contattare il nostro supporto tecnico 
                        <a href="mailto:info@kittenlovers.com">info@kittenlovers.com</a>.
                    </p>
                </div>
                <div class="col-lg-8">
                    <img class="img-responsive img-thumbnail" src="img/love-books.jpg"/>
                </div>
            </div>
        </div>
    </jsp:body>
</t:page_content>

<t:footer></t:footer>