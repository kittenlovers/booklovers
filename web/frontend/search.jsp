<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<t:header>
    <jsp:attribute name="pageTitle">Search</jsp:attribute>
</t:header>

<t:page_content>
    <jsp:body>
        <div class="page-header">
            <h1>Ricerca <small> ${param.title} | ${param.author} | ${param.genre} | ${param.publisher} | ${param.year} | ${param.profile}</small></h1>
        </div>      
        <div class="row grid">
            <div class="col-lg-3 col-md-4 col-sm-5 col-xs-9 grid-item">
                <form action="search" id="searchForm" method="POST" >
                    <label for="title">Titolo</label>
                    <input type="text" class="form-control" id="title" name="title" 
                           value="${param.title}" placeholder="Titolo" maxlength="64"><br/>
                    <label for="author">Autore</label>
                    <input type="text" class="form-control" id="author" name="author"
                           value="${param.author}" placeholder="Autore" maxlength="64"><br/>
                    <label for="year">Anno</label>
                    <input type="number" class="form-control" id="year" name="year"
                           value="${param.year}" placeholder="Anno"><br/>
                    <label for="publisher">Editore</label>
                    <input type="text" class="form-control" id="publisher" 
                           value="${param.publisher}" name="publisher" placeholder="Editore" maxlength="64"><br/>
                    <label for="genre">Genere</label>
                    <input type="text" class="form-control" id="genre" name="genre"
                           value="${param.genre}" placeholder="Genere" maxlength="64"><br/>
                    <label for="profile">Utente</label>
                    <input type="text" class="form-control" id="profile" name="profile"
                           value="${param.profile}" placeholder="Utente" maxlength="64"><br/>
                    <script type="text/javascript">
                        $(document).ready(function () {
                            $("#title").autocomplete({source: ${titles}});
                            $("#author").autocomplete({source: ${authors}});
                            $("#year").autocomplete({source: ${years}});
                            $("#publisher").autocomplete({source: ${publishers}});
                            $("#genre").autocomplete({source: ${genres}});
                            $("#profile").autocomplete({source: ${profiles}});
                        });
                    </script>
                    <button type="button" class="btn btn-default" onclick="clearForm(this.form);">Reset</button>
                    <button type="submit" class="btn btn-primary">Cerca</button>
                </form>
            </div>
            <c:forEach var="bk" items="${books}">
                <div class="col-lg-3 col-md-4 col-sm-5 col-xs-4 grid-item">
                    <t:book-thumb>
                        <jsp:attribute name="id">${bk.id}</jsp:attribute>
                        <jsp:attribute name="year">${bk.year}</jsp:attribute>
                        <jsp:attribute name="title">${bk.title}</jsp:attribute>
                        <jsp:attribute name="genre">${bk.genre}</jsp:attribute>
                        <jsp:attribute name="author">${bk.author}</jsp:attribute>
                        <jsp:attribute name="copertina">${bk.thumb}</jsp:attribute>
                        <jsp:attribute name="publisher">${bk.publisher}</jsp:attribute>
                        <jsp:attribute name="owner_id">${bk.owner.id}</jsp:attribute>
                        <jsp:attribute name="owner_username">${bk.owner.username}</jsp:attribute>
                    </t:book-thumb>
                </div>
            </c:forEach>
            <c:if test="${empty books}">
                <div class="col-lg-9 col-md-8 col-sm-7 col-xs-9 grid-item lead top-buffer-30">La ricerca non ha prodotto nessun risultato</div>
            </c:if>
        </div>
    </jsp:body>

</t:page_content>

<t:footer/>