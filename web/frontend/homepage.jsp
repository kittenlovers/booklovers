<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:header>
    <jsp:attribute name="pageTitle">BookLovers.com</jsp:attribute>
</t:header>

<t:page_content>
    <jsp:body>
        <div class="page-header">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="img/slide/slide1.jpg" class="img-responsive" alt="Books">
                        <div class="carousel-caption backg-alpha">
                            <h2 class="text-warning backg-alpha-light">BookLovers.com</h2>
                            <p class="lead">Cerca i tuoi libri preferiti!</p>
                        </div>
                    </div>
                    <div class="item"><img src="img/slide/slide2.jpg" class="img-responsive" alt="Books">
                        <div class="carousel-caption backg-alpha">
                            <h2 class="text-danger backg-alpha-light">BookLovers.com</h2>
                            <p class="lead">Condividi il tuo patrimonio librario!</p>
                        </div>
                    </div>
                    <div class="item"><img src="img/slide/slide3.jpg" class="img-responsive" alt="Books">                        
                        <div class="carousel-caption backg-alpha">
                            <h2 class="text-success backg-alpha-light">BookLovers.com</h2>
                            <p class="lead">Incontra persone con i tuoi stessi interessi!</p>
                        </div>
                    </div>
                </div>
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                <div class="jumbotron text-center">
                    <h2 class="col-lg-7">L'ultima frontiera del booksharing!</h2> 
                    <img class="img-responsive" alt="bookLovers.com" src="img/booklovers_logo-200.png"/>
                    <p class="lead">Un progetto di kittenlovers, <a href="about">scopri chi siamo</a></p>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <p class="lead">Ultimi libri inseriti</p>
                <ul class="media-list">
                    <c:forEach var="bk" items="${lastBooks}">
                        <li class="media">
                            <div class="media-left">
                                <a href="book?id=${bk.id}"><img class="media-object" src="${bk.thumb}" alt="Copertina"></a>
                            </div>
                            <div class="media-body media-middle">
                                <h4 class="media-heading"><a href="book?id=${bk.id}">${bk.title}</a></h4>
                            </div>
                        </li>
                    </c:forEach>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <h3 class="text-warning">Sharing</h3>
                <p>Su BookLovers.com hai la possibilita` di pubblicare il tuo patrimonio librario e di gestire una tua pagina personale</p>
            </div>
            <div class="col-sm-4">
                <h3 class="text-danger">Localizza</h3>
                <p>Su BookLovers.com puoi individuare utenti con i tuoi stessi interessi nelle tue vicinanze</p>
            </div>
            <div class="col-sm-4">
                <h3 class="text-success">Stalker</h3> 
                <p>Su BookLovers.com puoi fare dello stalking letterario!!</p>
            </div>
        </div>
    </jsp:body>

</t:page_content>

<t:footer/>